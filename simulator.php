<?php
require_once "functions.php";
$multiplier = 1000;

$command = $_GET['command'];
$x = (int) $_GET['x'];
$y = (int) $_GET['y'];
$result = array();
if ($command == 'row') {
    for ($i = $x-3; $i <= $x+3; $i++) {
        $result[] = createChunk( $i, $y);
    }
} else if ($command == 'col') {
    for ($i = $y-3; $i <= $y+3; $i++) {
        $result[] = createChunk( $x, $i);
    }
}

//echo json_encode(createChunk($x * $multiplier, $y * $multiplier));
echo json_encode($result);