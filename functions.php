<?php

function createId($x, $y, $prefix) {
    return $prefix . (($x > 0) ? $x : '_' . abs($x) ) . (($y > 0) ? $y : '_' . abs($y) );
}

function createChunk($x, $y) {
    $result = array();

    $result[] = createBuilding($x* 1000 +250, $y* 1000+250, createId($x, $y, 'b'));
    $result[] = createBuilding($x* 1000 +250, $y* 1000-250, createId($x, $y, 'b1'));
    $result[] = createBuilding($x* 1000 -250, $y* 1000+250, createId($x, $y, 'b2'));
    $result[] = createBuilding($x* 1000 -250, $y* 1000-250, createId($x, $y, 'b3'));
    //$result[] = createVehicle($x* 1000 + 420, $y* 1000 + 250, createId($x, $y, 'v1'));
    //$result[] = createVehicle($x* 1000 +420, $y* 1000 - 250, createId($x, $y, 'v2'));
    $result[] = createVehicle($x* 1000 -420, $y* 1000 - 250, createId($x, $y, 'v3'));

    $result[] = createWeapon($x* 1000 +410, $y* 1000+410, 'minigun', createId($x, $y, 'w'));
    $result[] = createWeapon($x* 1000 +410, $y* 1000-410, 'rocketLauncher', createId($x, $y, 'w1'));
    $result[] = createWeapon($x* 1000 -410, $y* 1000+410, 'flameThrower', createId($x, $y, 'w2'));
    $result[] = createWeapon($x* 1000 -410, $y* 1000-410, 'rocketLauncher', createId($x, $y, 'w3'));

    $roads = array();
    $roads[] = createStraightRoad($x* 1000, $y* 1000, 0);
    $roads[] = createStraightRoad($x* 1000, $y* 1000, 1.5707);
    $roads[] = createIntersection($x* 1000, $y* 1000);



    $chunk = new stdClass;
    $chunk->objects = $result;
    $chunk->roads = $roads;
    $chunk->x = $x;
    $chunk->y = $y;

    return $chunk;
}

function createIntersection($x, $y) {

    $road = new stdClass;
    $road->type = "intersection";
    $road->x = $x;
    $road->y = $y;

    return $road;
}

function createStraightRoad($x, $y, $rotation) {
    $road = new stdClass;
    $road->type = "straight";
    $road->x = $x;
    $road->y = $y;
    $road->rotation = $rotation;

    return $road;
}

function createWeapon($x, $y, $type, $id) {
    $weapon = createStub($x, $y, 'w', $id);
    $weapon->boundingBox = createBoundingBox(10,10);
    $weapon->boundary = createCircleBoundary(10);
    $weapon->subType = $type;

    return $weapon;
}

function createBuilding($x, $y, $id) {
    $building = createStub($x, $y, 'b', $id);
    $building->boundingBox = createBoundingBox(100,100);
    $building->boundary = createPolygonBoundary(100,100);
    return $building;
}

$colours = array(
    array(1.0, 0.03137254901960784, 0.19215686274509805),
    array(0.03137254901960784, 0.6470588235294118, 1.0),
    array(0.12549019607843137, 0.7176470588235294, 0.16470588235294117),
    array(1.0, 0.9686274509803922, 0.06274509803921569),
    array(0.4117647058823529, 0.07450980392156863, 0.6941176470588235)
);

function createVehicle($x, $y, $id) {
    $vehicle = createStub($x, $y, 'v', $id);
    $vehicle->boundingBox = createBoundingBox(50, 50);
    $vehicle->boundary = createPolygonBoundary(25,50);

    global $colours;
    $vehicle->colour = $colours[rand(0, count($colours)-1)];

    return $vehicle;
}

function createPerson($x, $y, $id) {
    $person = createStub($x, $y, 'p', $id);
    $person->boundingBox = createBoundingBox(15,15);
    $person->boundary = createPolygonBoundary(10,10);
    $person->state = "idle";

    return $person;
}

function createStub($x, $y, $type, $id) {
    $obj = new stdClass;
    $obj->id = $id;
    $obj->x = $x;
    $obj->y = $y;
    $obj->type = $type;

    return $obj;
}

function createCircleBoundary($radius) {
    $boundary = new stdClass;
    $boundary->radius = $radius;
    $boundary->type = 'circle';

    return $boundary;
}

function createPolygonBoundary ($width, $height, $points = null) {
    $boundary = new stdClass;
    $boundary->type = 'polygon';
    if ($points == null) {
        $boundary->points = array();
        $boundary->points[] = createPoint(-$width, $height);
        $boundary->points[] = createPoint($width, $height);
        $boundary->points[] = createPoint($width, -$height);
        $boundary->points[] = createPoint(-$width, -$height);
    } else {
        $boundary->points = $points;
    }
    return $boundary;
}

function createBoundingBox ($w, $h) {
    $box = new stdClass;
    $box->width = $w;
    $box->height = $h;

    return $box;
}

function createPoint($x, $y) {
    $p = new stdClass;
    $p->x = $x;
    $p->y = $y;

    return $p;
}