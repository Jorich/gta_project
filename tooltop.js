// This is a very simple implementation of a 'Tooltip'
var Tooltip = function(container) {
    var self = this;

    // add a simple tooltip to the provided container-element
    self.tooltip = container.append("div")
        .attr("class", "tooltip")
        .style("visibility", "hidden")
        .text("Drag me!");

    // functions to steer the visualisation of the tooltip
    self.showTooltip = function() {
        self.tooltip.style("visibility", "visible");
        d3.select(this).on("mousemove", self.moveTooltip);
    }
    self.hideTooltip = function() {
        self. tooltip.style("visibility", "hidden");
    }
    self.moveTooltip = function() {
        self.tooltip.style("top", (d3.event.pageY-10)+"px").style("left",(d3.event.pageX+10)+"px");
    }
}