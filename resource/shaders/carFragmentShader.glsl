uniform vec3 lightPosition;
uniform sampler2D texture;
uniform sampler2D mask;
uniform vec3 carColour;
varying vec3 interpolatedPosition;
varying vec3 interpolatedNormal;
varying vec2 interpolatedUv;

void main() {
  vec3 n = normalize(interpolatedNormal);
  vec3 l = normalize(lightPosition - interpolatedPosition);
  vec3 halfway = normalize(l - interpolatedPosition);

  vec3 texCol = vec3(texture2D(texture, interpolatedUv));
  vec3 maskCol = vec3(texture2D(mask, interpolatedUv));
  vec3 colour = maskCol * (texCol * carColour) + (1.0 - maskCol) * texCol;

  vec3 col =
    0.05 +
    colour * clamp(dot(n, l), 0.0, 1.0) +
    pow(clamp(dot(halfway, n), 0.0, 1.0), 220.0);

  gl_FragColor = vec4(col, 1.0);
}
