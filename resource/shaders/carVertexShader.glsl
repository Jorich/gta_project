varying vec3 interpolatedPosition;
varying vec3 interpolatedNormal;
varying vec2 interpolatedUv;

void main() {
  interpolatedPosition = (modelViewMatrix * vec4(position, 1.0)).xyz;
  interpolatedNormal = normalize(normalMatrix * normal);
  interpolatedUv = uv;
  gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
