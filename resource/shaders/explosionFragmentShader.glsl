	varying vec2 vUv;
	uniform vec3 yellow;
	uniform vec3 orange;
	uniform vec3 red;
	uniform vec3 black;
	uniform float darken;
	varying float noise;

	void main() {
		float wBlack;
		float wRed;
		float wOrange;
		float wYellow;

		vec3 color;
		float ceiling = 0.4;
		if(noise < 0.0) {
			color = black;
		} else if(noise < ceiling) {
			float upper = noise*(1.0/ceiling);
			
			wBlack = pow(1.0 - upper, 3.0);
			wRed = 3.0*upper*pow((1.0-upper), 2.0);
			wOrange = 3.0*pow(upper, 2.0)*(1.0-upper);
			wYellow = pow(upper, 3.0);
			color = wBlack*black + wRed*red + wOrange*orange + wYellow*yellow;
		} else {
			color = yellow;
		}
		color.r -= darken;
		color.g -= darken;
		color.b -= darken;
		gl_FragColor = vec4( color.rgb, 1.0 );

	}