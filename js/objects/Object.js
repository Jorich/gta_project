define([], function() {
    var Object = function(model) {
        this.id = null;
        this.x = null;
        this.y = null;

        this.boundary = {
            type: "none"
        };
        
        this.model = model;
        
        this.removeOnUpdate = false;

        this.segment = null;
        //this.addOrients();
    }

    Object.prototype.setXAndY = function(x, y) {
        this.y = y;
        this.x = x;
        if (this.camera) {
            this.camera.position.x = this.x;
            this.camera.position.y = this.y;
            sunLight.position.x = this.x + 1000;
            sunLight.position.y = this.y + 1000;
        }
    }

    Object.prototype.setRotation = function (rotation) {
        this.model.rotation.z = rotation;
        this.rotation = rotation;
    }

    Object.prototype.addOrients = function () {
        var geometry = new THREE.BoxGeometry(3, 3, 3);
        var green = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        var red = new THREE.MeshBasicMaterial( { color: 0xff0000 } );

        var x = new THREE.Mesh(geometry,red);
        var y = new THREE.Mesh(geometry,green);

        x.position.x = 50;
        y.position.y = 50;

        this.model.add(x).add(y);

    }

    Object.prototype.applySpec = function (spec) {
    	if(this.model.hasAnimations) {
    		this.model.builder.animationHandler(this, spec);
    	}
        var update = spec.x != this.x || spec.y != this.y;
        if (update) {
            this.setXAndY(spec.x, spec.y);
            if (this.segment) this.segment.updateObjectByPosition(this);
        }
        if (spec.radius != undefined && this.radius != spec.radius) { // FOR Explosion
            var scale = spec.radius / this.radius;
            this.model.scale.set(scale,scale,scale);
        }

        this.setRotation(spec.rotation);
    }

    Object.prototype.update = function () {
        count++;
        if (this.removeOnUpdate) {
            this.remove();
            return;
        }
        if (this.updateSpec) {
            this.applySpec(this.updateSpec);
            if (this.boundingBox) {
                this.updateBoundingBox(this.updateSpec.boundingBox);
            }
            this.updateSpec = null;
        }
    }

    Object.prototype.setSegment = function(segment) {
        this.segment = segment;
    }

    Object.prototype.addCamera = function(camera) {
        this.camera = camera;
    }

    Object.prototype.removeCamera = function() {
        this.camera = undefined;
    }

    Object.prototype.setModel = function(model) {
        this.model = model;
    }

    Object.prototype.addBoundaryPoints = function ( boundary) {
        if (!boundary) return;
        this.boundary.points = [];
        for(var i = 0 ; i < boundary.points.length; i++) {
            var geometry = new THREE.BoxGeometry(3, 3, 3);
            var green = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
            var mesh = new THREE.Mesh(geometry,green);
            mesh.position.z = 5;
            mesh.position.x = this.model.position.x - boundary.points[i].x;
            mesh.position.y = this.model.position.y - boundary.points[i].y;
            this.boundary.points.push(mesh);
            this.model.add(mesh);

        }
    }

    Object.prototype.addBoundaryCircle = function (boundary) {
        if(!boundary) return;
        this.boundary.radius = boundary.radius;

        var green = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        var circleGeometry = new THREE.CircleGeometry( this.boundary.radius, 10 );
        var circle = new THREE.Mesh( circleGeometry, green );
        circle.position.z = 5;
        this.model.add(circle);
        this.boundary.circle = circle;

    }

    Object.prototype.addBoundingBox = function (boundingBox) {
        var geometry = new THREE.PlaneGeometry( boundingBox.w, boundingBox.h);
        var material = new THREE.MeshBasicMaterial( {color: 0xffff00, side: THREE.DoubleSide} );
        this.boundingBox = new THREE.Mesh( geometry, material );

        this.boundingBox.position.set(this.x - boundingBox.x, this.y - boundingBox.y, 3);
        this.model.add(this.boundingBox);
    }

    Object.prototype.updateBoundingBox = function (boundingBox) {
        this.boundingBox.position.x = this.model.position.x - boundingBox.x;
        this.boundingBox.position.y = this.model.position.y - boundingBox.y;
        this.boundingBox.rotation.z = -this.model.rotation.z;
    }

    Object.prototype.updateBoundaryPoints = function (boundary) {
        for(var i = 0 ; i < boundary.points.length; i++) {
            this.boundary.points[i].position.x = this.model.position.x - boundary.points[i].x;
            this.boundary.points[i].position.y = this.model.position.y - boundary.points[i].y;
        }
    }

    Object.prototype.updateBoundaryCircle = function (boundary) {
        this.boundary.radius = boundary.radius;
    }

    Object.prototype.markObjectForRemoval = function () {
        this.removeOnUpdate = true;
    }

    Object.prototype.remove = function () {
    	if(this.model.builder) {
    		if(typeof this.model.builder.onDelete == 'function') {
    			this.model.builder.onDelete(this);
    		}
    	}
        if (this.boundary) {
            for (var i = 0; i < this.boundary.length; i++) {
                game.scene.remove(this.boundary[i]);
            }
        }
        if (this.boundingBox) {
            game.scene.remove(this.boundingBox);
        }
        if (this.segment) {
            this.segment.removeObject(this);
        }
        game.scene.remove(this.model);
        game.objectManager.remove(this);

    }

    return Object

})
