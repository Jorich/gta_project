define(['objects/model/Person',
        'objects/model/Car',
        'objects/model/Building',
        'objects/model/Rocket',
        'objects/model/Road',
        'objects/effect/Flame',
        'objects/effect/Smoke',
        'objects/effect/Explosion'
        ], function(Person, Car, Building, Rocket, Road, Flame, Smoke, Explosion) {
    var builder = function(manager) {
        this.manager = manager;
        this.personBuilder = new Person();
        this.carBuilder = new Car();
        this.buildingBuilder = new Building();
        this.flameBuilder = new Flame();
        this.smokeBuilder = new Smoke();
        this.explosionBuilder = new Explosion();
        this.rocketBuilder = new Rocket();
        this.roadBuilder = new Road();
    }

    builder.prototype.createPerson = function (specs) {
    	var model = this.personBuilder.getModel();
    	model.hasAnimations = true;
    	model.builder = this.personBuilder;
    	return model;
    }

    builder.prototype.buildMesh = function (g,m, z) {

        var mesh = new THREE.Mesh(g, m);
        mesh.applyMatrix( new THREE.Matrix4().makeTranslation( 0, 0, z ) );
        mesh.hasAnimations = false;
        return mesh;
    }

    builder.prototype.createBuilding = function (specs) {
    	var model = this.buildingBuilder.getModel();
    	model.hasAnimations = false;
    	model.builder = this.buildingBuilder;
    	return model;
    }

    builder.prototype.createRoad = function(specs) {
    	return this.roadBuilder.addStraightRoad(specs);
    }

    builder.prototype.createIntersection = function(specs) {
    	return this.roadBuilder.addIntersection(specs);
    }

    builder.prototype.createCurvedRoad = function(specs) {
        return this.roadBuilder.addCurvedRoad(specs);
    }

    builder.prototype.createVehicle = function (specs, addToManager) {
    	addToManager(this.carBuilder.getModel(specs));
    }

    builder.prototype.createWeapon = function (specs) {
        var geometry = new THREE.BoxGeometry(10, 15, 20);
        var material = new THREE.MeshBasicMaterial( { color: 0xf6f6f6 } );
        return this.buildMesh(geometry, material, 10);
    }

    builder.prototype.createExplosion = function (specs) {
    	var model = this.explosionBuilder.getModel();
    	model.hasAnimations = true;
    	model.builder = this.explosionBuilder;
    	return model;
    }

    builder.prototype.createProjectile = function (specs) {
        var geometry, material;

        if(specs.subType == "flame") {
        	var model = this.flameBuilder.getModel();
        	model.hasAnimations = true;
        	model.builder = this.flameBuilder;
        	return model;
        } else if(specs.subType == "rocket") {
        	var model = this.rocketBuilder.getModel();
        	model.hasAnimations = true;
        	model.builder = this.rocketBuilder;
        	return model;
        } else {
        	geometry = new THREE.BoxGeometry(2, 2, 2);
        	material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
        }
        return this.buildMesh(geometry, material, 9);
    }

    builder.prototype.smokeEffect = function (x, y, scale, duration) {
        this.smokeBuilder.create(x, y, scale, duration);
    }

    return builder;
});
