define([], function() {
	var Smoke = function(){
		Smoke.textures = [];
		for(var i = 0; i < 3; i++) {
			THREE.ImageUtils.loadTexture('resource/image/smoke/smoke' + i + '.png', THREE.UVMapping, this.onTextureLoaded);
		}
	}
	
	Smoke.prototype.onTextureLoaded = function (texture) {
		Smoke.textures.push(texture);
	}
	
	Smoke.prototype.create = function (x, y, scale, duration) {
		var randomTexture = Smoke.textures[Math.floor((Math.random() * 3))];
		var material = new THREE.SpriteMaterial({ map : randomTexture });
		material.transparent = true;
		var smoke = new THREE.Sprite(material);
		smoke.scale.x = 50*scale;
		smoke.scale.y = 50*scale;
		smoke.position.setX(x);
		smoke.position.setY(y);
		smoke.position.setZ(5);
		smoke.stage = 0;
		game.scene.add(smoke);
		setTimeout(function() {
		    Smoke.prototype.step(smoke);
		}, 60*duration)
	}
	
	Smoke.prototype.step = function(smoke) {
		if(++smoke.stage > 4) {
			game.scene.remove(smoke);
			delete smoke;
			return;
		}
		smoke.material.opacity -= 0.2;
		setTimeout(function() {
		    Smoke.prototype.step(smoke);
		}, 60)
	}
	
	return Smoke;
})
