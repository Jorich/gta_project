define([], function() {
	var Flame = function(){
		Flame.textures = [];
		for(var i = 0; i < 11; i++) {
			THREE.ImageUtils.loadTexture('resource/image/flame/flame' + i + '.png', THREE.UVMapping, this.onTextureLoaded);
		}
	}
	
	Flame.prototype.onTextureLoaded = function (texture) {
		Flame.textures.push(texture);
	}
	
	Flame.prototype.getModel = function () {
		var material = new THREE.SpriteMaterial({ 	map : Flame.textures[0],
													blending : THREE.AdditiveBlending,});
		var flame = new THREE.Sprite(material);
		flame.position.setZ(8);
		flame.scale.x = 50;
		flame.scale.y = 50;
		flame.stage = 0;
		flame.counter = 0;
		return flame;
	}
	
	Flame.prototype.animationHandler = function(current, spec) {
		current.model.counter += game.delta;	
		if(current.model.counter/50 > current.model.stage && current.model.stage < 10) {
			current.model.material.map = Flame.textures[++current.model.stage];
			current.model.material.map.needsUpdate = true;
			current.model.scale.x += current.model.stage/10;
			current.model.scale.y += current.model.stage/10;
		}
	}
	
	Flame.prototype.onDelete = function(object) {
		var x = object.x;
		var y = object.y;
		game.objectManager.modelBuilder.smokeEffect(x, y, 1, 1);
	}
	
	return Flame;
})
