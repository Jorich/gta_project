define([], function() {
	var Explosion = function(){
		var yellow = new THREE.Color(0xFFFF00);
		var orange = new THREE.Color(0xFFAF0A);
		var red = new THREE.Color(0xFF0000);
		var black = new THREE.Color(0x000000);
		
		$.when(
                $.ajax("/GTA_project/resource/shaders/explosionFragmentShader.glsl"),
                $.ajax("/GTA_project/resource/shaders/explosionVertexShader.glsl")
            ).done(function (frag, vert) {
            	Explosion.material = new THREE.ShaderMaterial( {
        			uniforms: { 
        				yellow: {
        					type: 'c',
        						value: yellow
        				},
        				orange: {
        					type: 'c',
        						value: orange
        				},
        				red: {
        					type: 'c',
        						value: red
        				},
        				black: {
        					type: 'c',
        						value: black
        				},
        				time: { 
        					type: "f", 
        					value: 0.0 
        				},
        				weight: { 
        					type: "f", 
        					value: 10.0 
        				},
        				darken: { 
        					type: "f", 
        					value: 0.0 
        				}
        			},
        			vertexShader: vert[0],
        			fragmentShader: frag[0]
        			
        		} );
            });
		
		
		Explosion.geometry = new THREE.SphereGeometry(10, 40, 40);
	}
	
	Explosion.prototype.getModel = function () {
		var material = Explosion.material.clone();
		material.uniforms['time'].value = Math.random();
		var model = new THREE.Mesh(Explosion.geometry, material);
        return model;
	}
	
	Explosion.prototype.animationHandler = function(current, spec) {
		current.model.material.uniforms['time'].value += .0001 * ( game.delta );
		if(spec.updateCount > 40) {
			current.model.material.uniforms['darken'].value = (spec.updateCount - 40)/20;
		}
	}
	
	Explosion.prototype.onDelete = function(object) {
		var x = object.x;
		var y = object.y;
		game.objectManager.modelBuilder.smokeEffect(x, y, 3, 5);
	}
	
	return Explosion;
})
