define([], function() {
	var Person = function(){
		var jsonloader = new THREE.JSONLoader();
		jsonloader.crossOrigin = 'Anonymous';
		jsonloader.load(
				'/GTA_project/resource/models/man.json',
				this.setGeoMat
		);
		
	}
	
	Person.prototype.setGeoMat = function (geometry, materials) {
		Person.geometry = geometry;
		Person.material = materials[0];
		//Person.material = new THREE.MeshBasicMaterial( { color: 0xAAAAAA } );
		Person.material.skinning = true;
	}
	
	Person.prototype.getModel = function () {
		var skinnedMesh = new THREE.SkinnedMesh(Person.geometry, Person.material);
		skinnedMesh.name = "innerMesh";
		skinnedMesh.animations = {};
		
		for (var i = 0; i < Person.geometry.animations.length; ++i) {
			var animName = Person.geometry.animations[i].name;
			//console.log('Loaded animation: ' + animName);
			skinnedMesh.animations[animName] = new THREE.Animation(skinnedMesh, Person.geometry.animations[i]);
			skinnedMesh.animations[animName].play();
			skinnedMesh.animations[animName].weight = 0;
		}
		skinnedMesh.scale.set(4, 4, 4);
		skinnedMesh.rotation.set(Math.PI/2, Math.PI, 0);
		skinnedMesh.position.set(0, 0, 10);
		var mesh = new THREE.Mesh();
		mesh.add(skinnedMesh);
        skinnedMesh.castShadow = true;
        skinnedMesh.receiveShadow = true;
		return mesh;
	}
	
	Person.prototype.animationHandler = function(current, spec) {
		var pModel = current.model.getObjectByName("innerMesh");	

		for(name in pModel.animations) {
			pModel.animations[name].weight = 0;
		}
		
		if(spec.state == "idle") {
			pModel.animations['idle'].weight = 1;
			return;
		}
		
		var up = false;
		var down = false;
		var left = false;
		var right = false;
		
		if(spec.y > current.y) {
			up = true;
		}
		else if (spec.y < current.y) {
			down = true;
		}
		if(spec.x > current.x) {
			right = true;
		}
		else if (spec.x < current.x){
			left = true;
		}
		
		var convertedRot = spec.rotation + Math.PI/2;
		var isDiagonal = false;
		if(up && right) {
			convertedRot += Math.PI/4;
			isDiagonal = true;
		}
		if(right && down) {
			convertedRot += Math.PI*(3/4);
			isDiagonal = true;
		}
		if(down && left) {
			convertedRot -= Math.PI*(3/4);
			isDiagonal = true;
		}
		if(left && up) {
			convertedRot -= Math.PI/4;
			isDiagonal = true;
		}
		
		if(!isDiagonal) {
			/*if(upPressed) {
				// do nothing, "base" case
			}*/
			if(right) {
				convertedRot += Math.PI/2;
			}
			if(down) {
				convertedRot += Math.PI;
			}
			if(left) {
				convertedRot -= Math.PI/2;
			}
		}

		var sin = Math.sin(convertedRot);
		var cos = Math.cos(convertedRot);

		if(sin > 0) {
			pModel.animations['run'].weight = Math.abs(sin*sin);
		}
		else {
			pModel.animations['back'].weight = Math.abs(sin*sin);
		}
		if(cos > 0) {
			pModel.animations['strafeL'].weight = Math.abs(cos*cos);
		}
		else {
			pModel.animations['strafeR'].weight = Math.abs(cos*cos);
		}
		// For debugging: displays forward, back, left, right weights.
		//console.log(pModel.animations['run'].weight + " " + pModel.animations['back'].weight + " " + pModel.animations['strafeL'].weight + " " + pModel.animations['strafeR'].weight);

	}
	
	return Person;
})
