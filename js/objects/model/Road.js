define([], function() {
	var Road = function(){
		var jsonloader = new THREE.JSONLoader();
		jsonloader.crossOrigin = 'Anonymous';
		THREE.ImageUtils.loadTexture('resource/image/road.jpg', THREE.UVMapping, function(roadTex) {
			THREE.ImageUtils.loadTexture('resource/image/road-intersection.jpg', THREE.UVMapping, function(roadTexNoCenter) {
				jsonloader.load('resource/models/intersection.json', function(model1) {
					roadTex.wrapS = roadTex.wrapT = THREE.RepeatWrapping;
					Road.roadTex = roadTex;
                    Road.roadTex.anisotropy = 16;
					Road.roadTexNoCenter = roadTexNoCenter;
					Road.IntersectionModel = model1;
				});
			});
		});

        this.width = 180;
	}

	Road.prototype.addStraightRoad = function (specs) {
		var tex = Road.roadTex.clone();
		tex.needsUpdate = true;
		var length = 1000;
		if(specs) {
			if(specs.length) length = specs.length;
		}
		tex.repeat.set(1, length / this.width);
		var geometry = new THREE.PlaneGeometry(this.width, length);
		var material = createMaterial(tex);
		var mesh = new THREE.Mesh(geometry, material);
        mesh.receiveShadow = true;
        mesh.castShadow = false;
        return mesh;
	}

	Road.prototype.addIntersection = function () {
		var s = this.width / 2;
		var model = Road.IntersectionModel.clone();
		model.applyMatrix(new THREE.Matrix4().makeRotationX(1.57079633).multiply(new THREE.Matrix4().makeScale(s, s, s)));
		var material = createMaterial(Road.roadTexNoCenter);
		var mesh = new THREE.Mesh(model, material);
        mesh.receiveShadow = true;
        mesh.castShadow = false;
		mesh.position.z = 0.001
		return mesh;
	}

    Road.prototype.addCurvedRoad = function (specs) {
        if (!specs.points)
            alert("No curved road points!");

        var spline = new THREE.SplineCurve3();
        for (var i = 0; i < specs.points.length; i++) {
            spline.points.push(new THREE.Vector3(specs.points[i][0], specs.points[i][1], 0));
        }

        var material = new THREE.MeshBasicMaterial({map: Road.roadTex});
        var stepLen = 60;
        var splineLen = spline.getLength();
        var steps = Math.floor(splineLen / stepLen);
        var geom = new THREE.Geometry();

        // Create vertices
        var rotMat = new THREE.Matrix4().makeRotationZ(1.57079633);
        var d = 1 / steps;
        var t = 0;
        for (var i = 0; i < steps; i++) {
            var point = spline.getPoint(t);
            var tangent = spline.getTangent(t).applyMatrix4(rotMat).multiplyScalar(this.width / 2);

            geom.vertices.push(point.clone().sub(tangent));
            geom.vertices.push(point.clone().add(tangent));

            t += d;
        }

        // Create faces
        for (var i = 0; i < steps * 2 - 3; i += 2) {
            geom.faces.push(new THREE.Face3(i + 1, i, i + 2));
            geom.faces.push(new THREE.Face3(i + 1, i + 2, i + 3));

            var yl = geom.vertices[i + 1].distanceTo(geom.vertices[i + 3]) / this.width;
            var yr = geom.vertices[i].distanceTo(geom.vertices[i + 2]) / this.width;

            geom.faceVertexUvs[0].push([new THREE.Vector2(0, 0),
                                        new THREE.Vector2(1, 0),
                                        new THREE.Vector2(1, yr)]);
            geom.faceVertexUvs[0].push([new THREE.Vector2(0, 0),
                                        new THREE.Vector2(1, yr),
                                        new THREE.Vector2(0, yl)]);
        }

        var mesh = new THREE.Mesh(geom, material);
        //mesh.receiveShadow = true;
        mesh.position.z = 0.005;

        if (specs.visualise) {
            var geometry = new THREE.Geometry();
            var d = 1 / steps;
            var t = 0;
            for (var i = 0; i < steps; i++) {
                geometry.vertices.push(spline.getPoint(t));
                t += d;
            }

            mesh.add(new THREE.PointCloud(geometry, new THREE.PointCloudMaterial({ size: 10, color: '#ff0000' })));
        }

        return mesh;
    }

    var createMaterial = function (tex) {
        return new THREE.MeshPhongMaterial({map: tex, shading: THREE.FlatShading, shininess: 0});
    }

	return Road;
})
