define([], function() {
	var Rocket = function(){
		Rocket.geometry = new THREE.BoxGeometry(4, 4, 4);
		Rocket.material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
	}
	
	Rocket.prototype.getModel = function () {
		return new THREE.Mesh(Rocket.geometry, Rocket.material);
	}
	
	Rocket.prototype.animationHandler = function(current, spec) {
		setTimeout(function() {
			game.objectManager.modelBuilder.smokeEffect(spec.x, spec.y, 0.3, 1);
		}, 30)
		
	}
	
	return Rocket;
})
