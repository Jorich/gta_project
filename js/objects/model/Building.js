define([], function() {
	var Building = function(){
		Building.texture = new THREE.ImageUtils.loadTexture('/GTA_project/resource/image/building.jpg');
		Building.material = new THREE.MeshPhongMaterial({map: Building.texture, shading: THREE.FlatShading});
		var jsonloader = new THREE.JSONLoader();
		jsonloader.crossOrigin = 'Anonymous';
		jsonloader.load(
				'/GTA_project/resource/models/building.json',
				this.setGeo
		);
		
	}
	
	Building.prototype.setGeo = function (geometry, materials) {
		Building.geometry = geometry;
	}
	
	Building.prototype.getModel = function () {
		var innerMesh = new THREE.Mesh(Building.geometry, Building.material);
		innerMesh.rotation.set(Math.PI/2, 0, 0);
		innerMesh.scale.set(1.5, 1.5, 1.76);
        innerMesh.castShadow = true;
        innerMesh.receiveShadow = false;
		var mesh = new THREE.Mesh();
		mesh.add(innerMesh);

		return mesh;
	}
	
	return Building;
})
