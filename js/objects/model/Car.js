define([], function() {
	var Car = function(){
		var jsonloader = new THREE.JSONLoader();
		jsonloader.crossOrigin = "Anonymous";
        jsonloader.load("/GTA_project/resource/models/gt.json",
            function (carGeom, ignored) {
                jsonloader.load('/GTA_project/resource/models/gt-wheel.json',
                    function(wheelGeom, ignored) {
                        $.when(
                            $.ajax("/GTA_project/resource/shaders/carFragmentShader.glsl"),
                            $.ajax("/GTA_project/resource/shaders/carVertexShader.glsl")
                        ).done(function (frag, vert) {
                        	Car.fragmentShader = frag[0];
                        	Car.vertexShader = vert[0];
                        	var wheelTex = new THREE.ImageUtils.loadTexture('/GTA_project/resource/image/gt-wheel.png');
                        	Car.carTex = new THREE.ImageUtils.loadTexture('/GTA_project/resource/image/gt.png');
                            Car.carMask = new THREE.ImageUtils.loadTexture('/GTA_project/resource/image/gt-mask.png');
                        	Car.carGeom = carGeom;
                        	Car.wheelGeom = wheelGeom;
                            Car.wheelMat = new THREE.MeshBasicMaterial( { map: wheelTex } );

                            // Scale factor
                            Car.s = 28;
                            var transform = new THREE.Matrix4().makeRotationX(1.57079633).multiply(new THREE.Matrix4().makeScale(Car.s, Car.s, Car.s));

                            Car.carGeom.applyMatrix(transform);
                            Car.wheelGeom.applyMatrix(transform);
                        });
                    });
            });

	}

	Car.prototype.getModel = function (specs) {
		material = new THREE.ShaderMaterial({
            uniforms: {
                texture: { type: 't', value: Car.carTex },
                mask: { type: 't', value: Car.carMask },
                lightPosition: { type: 'v3', value: new THREE.Vector3(0, 0, 1000) },
                carColour: { type: 'v3', value: new THREE.Vector3(specs.colour[0], specs.colour[1], specs.colour[2]) }
            },
            vertexShader: Car.vertexShader,
            fragmentShader: Car.fragmentShader
        });

		var carMesh = new THREE.Mesh(Car.carGeom, material);
        carMesh.castShadow = true;

        var car = new THREE.Object3D();
        car.add(carMesh);

        var wheelMesh;
        var s = Car.s;

        // Front right
        wheelMesh = new THREE.Mesh(Car.wheelGeom, Car.wheelMat);
        wheelMesh.position.x = s * 0.76;
        wheelMesh.position.y = s * 1.02;
        wheelMesh.position.z = s * 0.33;
        wheelMesh.castShadow = true;
        car.add(wheelMesh);

        // Front left
        wheelMesh = new THREE.Mesh(Car.wheelGeom, Car.wheelMat);
        wheelMesh.position.x = s * -0.76;
        wheelMesh.position.y = s * 1.02;
        wheelMesh.position.z = s * 0.33;
        wheelMesh.rotation.z = Math.PI;
        wheelMesh.castShadow = true;
        car.add(wheelMesh);

        // Rear right
        wheelMesh = new THREE.Mesh(Car.wheelGeom, Car.wheelMat);
        wheelMesh.position.x = s * 0.76;
        wheelMesh.position.y = s * -1.37;
        wheelMesh.position.z = s * 0.33;
        wheelMesh.castShadow = true;
        car.add(wheelMesh);

        // Rear left
        wheelMesh = new THREE.Mesh(Car.wheelGeom, Car.wheelMat);
        wheelMesh.position.x = s * -0.76;
        wheelMesh.position.y = s * -1.37;
        wheelMesh.position.z = s * 0.33;
        wheelMesh.rotation.z = Math.PI;
        wheelMesh.castShadow = true;
        car.add(wheelMesh);

        car.hasAnimations = false;

        return car;
	}

	return Car;
})
