var sunLight;

define([
        'game/Player',
        'game/Map',
        'game/core/object/Resources',
        'objects/Object',
        'game/core/object/Manager',
        'game/core/message/Manager',
        'game/core/controls/ControlInputManager'
    ],
    function(Player, Map, Resources, Object, ObjectManager, MessageManager, Controls) {
    var Game = function() {
        this.initParamsForStats();
        this.player = new Player();
        this.resources = new Resources();
        this.map = new Map();

        this.objectManager = new ObjectManager();
        this.objectManager.setResources(this.resources);
        this.objectManager.setMap(this.map);

        this.worker = new Worker('js/game/Worker.js');
        this.messageManager = new MessageManager(this.worker);
        this.messageManager.setObjectManager(this.objectManager);
        this.messageManager.setPlayer(this.player);

        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 1000 );
        this.camera.position.z = 450;

    }

    Game.prototype.init = function() {
        this.map.init();
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setSize( window.innerWidth-10, window.innerHeight-50 );

        this.renderer.shadowMapEnabled = true;
        this.renderer.shadowMapType = THREE.PCFSoftShadowMap;

        var ambient = new THREE.AmbientLight(0x444444);
        this.scene.add(ambient);

        sunLight = new THREE.DirectionalLight(0xffffff, 1);
        sunLight.castShadow = true;
        sunLight.shadowDarkness = 0.5;
        sunLight.shadowCameraNear = 0.01;
        sunLight.target = this.camera;
        sunLight.position.set(1000, 1000, 3000);
        sunLight.shadowCameraLeft = -1000;
        sunLight.shadowCameraRight = 1000;
        //sunLight.shadowMapWidth = 1024;
        //sunLight.shadowMapHeight = 1024;
        this.scene.add(sunLight);

        document.body.appendChild( this.renderer.domElement );
        setTimeout(function() {
            game.messageManager.init();
        },1000);

        this.initStats();
        //this.addOrients();

    }

    Game.prototype.initParamsForStats = function () {
        this.fps = 60;
        this.then = Date.now();
        this.interval = 1000 / this.fps;
        this.delta = 0;
    }

    Game.prototype.setupPlayer = function (object) {
        this.player.setObject(object);
        var controls = new Controls(this.worker, object)
        this.player.setControls(controls);
        this.player.attachCamera(game.camera);
        controls.init();
    }

    Game.prototype.updatePlayer = function (object) {
        this.player.removeCamera(game.camera);
        this.player.setObject(object);
        this.player.controls.setObject(object);
        this.player.attachCamera(game.camera);
    }

    Game.prototype.test = function() {
        //var controls = new THREE.OrbitControls( this.camera);
        //controls.addEventListener( 'change', this.render );

        /*var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        var material2 = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
        var geometry = new THREE.BoxGeometry( 10, 10, 10 );
        var geometry2 = new THREE.BoxGeometry( 5, 5, 5 );
        ping = true;
        */
        /*parent1 = new THREE.Mesh( geometry, material );
        parent1.position.x = -100;
        parent2 = new THREE.Mesh( geometry, material );
        parent2.position.x = 100;
        child = new THREE.Mesh( geometry2, material2 );
        child.position.z = 50;
        child.position.x = -50;
        //parent2.add(child);
        this.scene.add(parent1).add(parent2).add(child);
        */
        /*setInterval(function() {

            if (ping) {
                parent1.remove(child);

                console.log("SHOULD BE 50");
                //console.log(child.position);
                var a = parent1.localToWorld(child.position.clone());
                parent2.add(child);
                child.position = parent2.worldToLocal(a.clone());
                child.updateMatrixWorld();
                console.log(child.position);
            } else {
                parent2.remove(child);
                parent1.add(child);
                var a = parent2.localToWorld(child.position.clone());
                parent1.add(child);
                child.position = parent1.worldToLocal(a.clone());
                child.updateMatrixWorld();
                console.log(child.position);
            }

            ping = !ping;
        }, 1000);*/


    }

    Game.prototype.addOrients = function() {
        var geometry = new THREE.BoxGeometry(3, 3, 3);
        var green = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
        var red = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
        var blue = new THREE.MeshBasicMaterial( { color: 0x0000ff } );

        var x = new THREE.Mesh(geometry,green);
        var y = new THREE.Mesh(geometry,red);
        var z = new THREE.Mesh(geometry,blue);

        x.position.x = 30;
        y.position.y = 30;
        z.position.z = 30;
        this.scene.add(x).add(y).add(z);
    }

    Game.prototype.initStats = function () {
        this.stats = new Stats();
        this.stats.setMode(0); // 0: fps, 1: ms

        // align top-left
        this.stats.domElement.style.position = 'absolute';
        this.stats.domElement.style.left = '0px';
        this.stats.domElement.style.top = '0px';

        this.engineStats = new Stats();
        this.engineStats.setMode(1);
        this.engineStats.domElement.style.position = 'absolute';
        this.engineStats.domElement.style.right = '0px';
        this.engineStats.domElement.style.top = '0px';
        document.body.appendChild(this.engineStats.domElement);
        document.body.appendChild( this.stats.domElement );
    }

    Game.prototype.render = function() {
        requestAnimationFrame( game.render );
        game.now = Date.now();
        game.delta = game.now - game.then;
        if (game.delta > game.interval) {
        	THREE.AnimationHandler.update(game.delta/600);
            game.stats.begin();

            game.renderer.render(game.scene, game.camera);
            game.stats.end();
            game.then = game.now - (game.delta % game.interval);

        }
    }

    Game.prototype.run = function() {
        this.render();
        var self = this;
        setInterval(function() {
            game.engineStats.begin();
            if (game.player.controls) {
                game.player.controls.registerMousePosition();
                game.player.controls.postMessage();
            }
            count =0;
            countFromSegment = 0;
            removeMessageCount = 0;
            manageRemoveCount =0;
            game.map.update();
            //console.log('nr. of elements on main: ' + count);
            /*console.log('nr of removeMessages: ' + removeMessageCount);
             /*console.log('nr. of elements on segment: ' + countFromSegment);
             console.log('nr. of elements on manager remove: ' + manageRemoveCount);*/
            game.engineStats.end();
        }, 5);
    }

    return Game;
})
