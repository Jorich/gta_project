require.config({
    paths: {
        three: 'lib/three.min',
        jQuery: 'lib/jquery-1.11.2',
        stats: 'lib/stats.min'
        //orbitControl: 'https://dl.dropboxusercontent.com/u/3587259/Code/Threejs/OrbitControls', //'lib/OrbitControls'

    },
    shim: {
    }
});
require([
    'Game',
    'game/core/worker/SegmentManager',
    'three',
    'jQuery',
    'stats'
], function (Game, SegmentManager) {

    game = new Game();
    game.init();
    //game.test();
    game.run();

    /*player = {
        object: {
        x:0,
        y:0
    }
    }
    player.object.x = -1500;
    player.object.y = -1500;
    segmentManager = new SegmentManager({}, {});
    segmentManager.init();
    setInterval(function() {
        segmentManager.update();
        player.object.x -= 100;
        player.object.y -= 100;
        console.log('asd '+ player.object.x + " : " + player.object.y);
        segmentManager.print();
    }, 2000)*/
});
