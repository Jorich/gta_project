define([], function () {
    var o = function (id) {
        this.id = id;
        this.type="abstract";
        console.log("Abstract const called");
    };

    o.prototype.print = function () {
        console.log(this.id);
    }

    return o;
})
