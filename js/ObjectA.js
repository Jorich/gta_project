define(['AbstractObject'], function (AbstractObject) {
    var _super = AbstractObject.prototype;
    var ObjectA = function(id){
        AbstractObject.call(this, id);
        this.type="a";
        console.log("A const called");
    };
    ObjectA.prototype = Object.create(_super);
    ObjectA.prototype.a = function () {
        console.log('A func called')
    }

    return ObjectA;
})
