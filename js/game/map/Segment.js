define([], function() {
    var Segment = function(x, y, coordinateMultiplier) {
        this.x = x;
        this.y = y;
        this.floor = null;
        this.coordinateMultiplier = coordinateMultiplier;
        this.g_x = this.x * coordinateMultiplier;
        this.g_y = this.y * coordinateMultiplier;
        this.objects = [];
        this.roads = [];
        //this.object = new THREE.Group();
        //this.object.position.x = x * coordinateMultiplier;
        //this.object.position.y = y * coordinateMultiplier;

        this.inScene = false;
        this.neighbours = [];
    }

    Segment.prototype.init = function() {
        this.floor = new THREE.Mesh(game.resources.get('map_floor_geometry'), game.resources.get('map_floor_material'));
        this.floor.receiveShadow = true;
        this.floor.castShadow = false;
        this.floor.position.x = this.g_x;
        this.floor.position.y = this.g_y;
        //this.object.add(this.floor);
        game.scene.add(this.floor);
        for (var i = 0; i < this.roads.length; i++) {
            game.scene.add(this.roads[i]);
        }

    }

    Segment.prototype.shouldBeInScene = function(x,y) {
        return Math.abs(x - this.x) <= 1 && Math.abs(y - this.y) <= 1;
    }

    Segment.prototype.shouldBeLoaded = function(x,y) {
        return Math.abs(x - this.x) <= 3 && Math.abs(y - this.y) <= 3;
    }

    Segment.prototype.update = function() {
        //console.log('asdasdasd');
        var x = parseInt(game.camera.position.x / this.coordinateMultiplier);
        var y = parseInt(game.camera.position.y / this.coordinateMultiplier);
        /*if (!this.shouldBeLoaded(x, y)) {
            this.unload();
        }*/

        if (this.shouldBeInScene(x, y)) {
            if (!this.inScene) {
                for (var i = 0; i< this.objects.length; i++) {
//                    game.scene.add(this.objects[i].model);
                    this.objects[i].model.visible = true;
                }
                for (var i = 0; i < this.roads.length; i++) {
                    this.roads[i].visible = true;
                }
                //game.scene.add(this.object);
                this.inScene = true;
                this.floor.visible=true;
            }
            if (this.inScene) {
                for (var i = 0; i< this.objects.length; i++) {
                    this.objects[i].update();
                }
            }
        } else {
            for (var i = 0; i< this.objects.length; i++) {
                //game.scene.remove(this.objects[i].model);
                this.objects[i].model.visible = false;
            }
            for (var i = 0; i < this.roads.length; i++) {
                this.roads[i].visible = false;
            }
            //game.scene.remove(this.object);
            this.inScene = false;
            this.floor.visible=false;
        }
    }

    Segment.prototype.addObject = function(object) {
        var t_x = Math.abs((object.x == null ? 0 : object.x) - this.g_x);
        var t_y = Math.abs((object.y == null ? 0 : object.y) - this.g_y);
        var crit = this.coordinateMultiplier / 2;
        if (t_x <= crit && t_y <= crit) {
            //this.object.add(object.model);
            //console.log('Object added: ' +  object.x + " : " + object.y);
            this.objects.push(object);
            object.setSegment(this);
            //game.scene.add(object.model);
        } else {
            this.pushObjectToClosestNeighbour(object);
        }
    }

    Segment.prototype.setObjectModelPosition = function (object) {
        //var a = this.object.worldToLocal(new THREE.Vector3(object.x,object.y,object.model.position.z));
        //object.model.position.x = a.x;
        //object.model.position.y = a.y;
        //console.log(object.id);
        object.model.position.x = object.x;
        object.model.position.y = object.y;
        //console.log(object.model.position);

    }

    Segment.prototype.updateObjectByPosition = function(object) {
        var t_x = Math.abs(object.x - this.g_x);
        var t_y = Math.abs(object.y - this.g_y);
        var crit = this.coordinateMultiplier / 2;
        if (t_x > crit || t_y > crit) {
            this.removeObject(object);
            //console.log("Pushing");
            this.pushObjectToClosestNeighbour(object);
        } else {
            this.setObjectModelPosition(object);
        }
    }

    Segment.prototype.pushObjectToClosestNeighbour = function(object) {
        var minDistance = null;
        var closest = null;

        for (var i = 0; i < this.neighbours.length; i++) {
            var candidate = this.neighbours[i];
            var distance = Math.sqrt(
                Math.pow(object.x - candidate.g_x, 2) +
                Math.pow(object.y - candidate.g_y, 2)
            );
            if (minDistance == null || distance < minDistance) {
                minDistance = distance;
                closest = candidate;
            }
        }


        if (closest != null) {
            //console.log(object.x + " : " + object.y +" mindist( " + minDistance  + " ) " + closest.g_x + " : " + closest.g_y);
            closest.addObject(object);
        } else {
            console.log(object);
            alert("We are losing an object ! ");
        }
    }

    Segment.prototype.removeObject = function(object) {
        //this.object.remove(object.model);
        //var a = this.object.localToWorld(object.model.position.clone());
        //object.model.position.x = a.x;
        //object.model.position.y = a.y;

        var i = this.objects.indexOf(object);
        if (i > -1) {
            //console.log('removing object');
            //console.log(object);
            //game.scene.remove(this.objects[i].model);
            this.objects.splice(i,1);
        } else {
            console.log("Can't remove an object, undefined index");
        }

    }

    Segment.prototype.corresponds = function(x, y) {
        return this.x == x && this.y == y;
    }

    Segment.prototype.isNeighbour = function(segment) {
        return Math.abs(this.x - segment.x) < 2 && Math.abs(this.y - segment.y) < 2;
    }

    Segment.prototype.addIfNeighbour = function(segment) {
        if (this.isNeighbour(segment)) {
            this.neighbours.push(segment);
        }
    }

    Segment.prototype.unload = function() {
        for (var i = 0; i < this.objects.length; i++) {
            this.objects[i].remove();
        }
        game.scene.remove(this.floor);
        game.map.removeSegment(this);
    }

    Segment.prototype.addRoad = function(road) {
        this.roads.push(road);
    }

    return Segment;
});
