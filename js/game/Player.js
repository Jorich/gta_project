define(['game/core/controls/ControlInputManager'], function(Controls) {

    var Player = function() {
    }

    Player.prototype.init = function() {
        this.controls.init();
    }

    Player.prototype.setControls = function(controls) {
        this.controls = controls;
    }

    Player.prototype.setObject = function (object) {
        this.object = object;
    }

    Player.prototype.removeCamera = function () {
        this.object.removeCamera();
    }

    Player.prototype.attachCamera = function (camera) {
        if (!this.object) alert('Attaching a camera to nonexistant object');
        this.object.addCamera(camera);
    }

    Player.prototype.update = function(dt) {

    }

    return Player;
});
