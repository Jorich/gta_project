define(['game/map/Segment'], function(Segment) {
    var Map = function() {
        this.segments = [];
        this.centerSegment = null;
        this.coordinateMultiplier = 1000;
        this.lattice = {};
    }


    Map.prototype.getKeyForLattice = function(x,y) {
        return this.getKeyPartForLattice(x) + "_" + this.getKeyPartForLattice(y);
    }
    Map.prototype.getKeyPartForLattice = function (coordinate) {
        return coordinate >= 0 ? coordinate : '_' + (- coordinate);
    }

    Map.prototype.init = function() {
        this.loadResources();
       /* for (var i = -10; i <= 10; i++) {
            for (var j = -10; j <= 10; j++) {
                if (i == 0 && j == 0) {
                    this.centerSegment = this.createSegment(i,j);
                } else {
                    this.createSegment(i,j);
                }
            }
        }*/
    }

    Map.prototype.createSegment = function (specs) {
        var seg = new Segment(specs.x,specs.y,this.coordinateMultiplier);
        if (specs.roads) {
            for ( var i = 0; i < specs.roads.length; i++) {

                seg.addRoad(game.objectManager.createRoad(specs.roads[i]));
            }
        }
        seg.init();
        console.log('adding segment: '+ seg.x + " : " + seg.y);
        var added = false;
        for (var i = 0 ; i < this.segments.length; i++) {
            if (this.segments[i].isNeighbour(seg)) {
                added = true;
                this.segments[i].addIfNeighbour(seg);
                seg.addIfNeighbour(this.segments[i]);
            }
        }
        if (!(!added || this.segments.length != 0)) {
            console.log("losing segment");
            console.log(seg);
        }
        this.segments.push(seg);

        this.lattice[this.getKeyForLattice(seg.x, seg.y)] = seg;

        return seg;
    };

    Map.prototype.loadResources = function () {
        var floorTexture = new THREE.ImageUtils.loadTexture('resource/image/grassTile01.jpg' );
        floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping;
        floorTexture.repeat.set( 10, 10 );
        game.resources.set('map_floor_material',  new THREE.MeshPhongMaterial( { map: floorTexture, side: THREE.FrontSide, shading: THREE.FlatShading, shininess: 0 } ))
        var floorMesh = new THREE.PlaneGeometry(this.coordinateMultiplier, this.coordinateMultiplier, 10, 10);
        game.resources.set('map_floor_geometry', floorMesh);
    }

    Map.prototype.update = function () {
        for (var i = 0; i < this.segments.length; i++) {
            var segment = this.segments[i];
            segment.update();
        }
    }

    Map.prototype.getSegment = function(x, y) {
        for (var i = 0; i < this.segments.length ; i++) {
            if (this.segments[i].corresponds(x,y)) {
                return this.segments[i];
            }
        }

        return null;
    }

    Map.prototype.loadSegments = function(coords) {
        game.messageManager.loadSegments(coords);
    }

    Map.prototype.removeSegment = function(segment) {
        var i = this.segments.indexOf(segment);
        if (i > -1) {
            this.segments.splice(i,1);
            console.log("REMOVING SEGMENT");
            this.lattice[this.getKeyForLattice(segment.x, segment.y)] = null;
            delete this.lattice[this.getKeyForLattice(segment.x, segment.y)];

        } else {
            alert('Unload segment failed');
        }
    }

    Map.prototype.getLatticeCoords = function(coord) {
        return parseInt(coord / 1000) + (coord % 1000 != 0 ? (Math.abs(coord %1000) > 500 ? 1 * ((coord >= 0) ? 1 : -1 ) : 0) : 0);
    };

    Map.prototype.add = function (object) {
        var key = this.getKeyForLattice(this.getLatticeCoords(object.x),this.getLatticeCoords(object.y));
        var segment = this.lattice[key];
        if (segment) {
            game.scene.add(object.model);
            segment.addObject(object);
        } else {
            console.log("Object for non existent Segment! " + key + " -----" + object.x + " : " + object.y);
        }
        //this.centerSegment.addObject(object);
    }

    Map.prototype.process = function(message)  {

        if (message.subType == 'remove') {
            var key = this.getKeyForLattice(message.specs.x,message.specs.y);
            var segment = this.lattice[key];
            if (segment) {
                segment.unload();
            }
        }

        if (message.subType == 'create') {
            //console.log(message.specs);
            if (this.centerSegment == null) {
                this.centerSegment = this.createSegment(message.specs);
            } else {
                this.createSegment(message.specs);
            }
        }
    }

    Map.prototype.print = function () {
        for (var i = 0; i < this.segments.length; i+=7) {
            var message = '';
            for (var j = 0; j < 7; j++) {
                message += this.segments[i+j].x + ":" + this.segments[i+j].y  + '|';
            }
            console.log(message);
        }
    }

    return Map;
})
