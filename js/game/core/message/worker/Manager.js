define([], function() {
    var manager = function() {
        this.objectManager = null;
        this.controls = null;
        this.player = null;
    }

    manager.prototype.setControls = function(controls) {
        this.controls = controls;
    }

    manager.prototype.setPlayer = function(player) {
        this.player = player;
    }

    manager.prototype.setObjectManager = function (objectManager) {
        this.objectManager = objectManager;
    }

    manager.prototype.init = function() {
        var that = this;
        self.onmessage = function(e) {
            var message = e.data;
            if (message.type == "control") {
                that.controls.process(message);
                return;
            }
            if (message.type == "init"){
                //postMessage('Initing stuff');
                /*that.objectManager.loadObjects(that.onObjectsLoaded);
                that.objectManager.createPlayerObject(that.onPlayerLoaded);*/
                segmentManager.init();
            }
        }
    }

    manager.prototype.postObjects = function(objects) {
        postMessage({type: "object", subType: "batch_create", objects: this.objectManager.objectsToSpec(objects)});
    }

    manager.prototype.postObject = function (object) {
        postMessage({type: "object", subType: "create", specs: object.toSpec()});
    }

    manager.prototype.onPlayerLoaded = function (object) {
        this.player.setObject(object);
        postMessage({type: "initPlayer", subType: "create", specs: object.toSpec()});
    }
    manager.prototype.onObjectsLoaded = function(objects) {
        //messageManager.postObjects(objects);
    }

    return manager;
});
