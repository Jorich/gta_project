define([],function() {
    var manager = function(worker){
        this.worker = worker;
        this.objectManager = null;

    }

    manager.prototype.init = function () {
        this.setupMessageListeners();
        this.callInitMessage();
    }

    manager.prototype.setObjectManager = function (manager) {
        this.objectManager = manager;
    }

    manager.prototype.setMap = function (map) {
        this.map = map;
    }

    manager.prototype.setPlayer = function(player) {
        this.player = player;
    }

    manager.prototype.callInitMessage = function () {
        this.worker.postMessage({type: "init"});
    }

    manager.prototype.setupMessageListeners = function() {
        var self = this;
        this.worker.onmessage = function (e) {
            //console.log(e.data);
            var message = e.data;
            if (message.type == "object") {
                self.objectManager.process(message);
            }
            if (message.type == 'map') {
                game.map.process(message);
            }

            if (message.type == "initPlayer") {
                if (message.subType == "create") {
                    message.type = 'object';
                    var object = self.objectManager.process(message);
                    game.setupPlayer(object);
                } else if (message.subType == "update") {
                    var object = self.objectManager.objects[message.specs.id];
                    game.updatePlayer(object);
                }

            }
        }
    }

    return manager;
});
