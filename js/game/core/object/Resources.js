define([], function() {
    var Resources = function() {
        this.container = {}
    }

    Resources.prototype.set = function(key, value) {
        this.container[key] = value;
    }

    Resources.prototype.get = function(key) {
        return this.container[key];
    }

    Resources.prototype.init = function() {

    }

    return Resources;
})
