define(["game/core/object/Resources", "objects/Object", "objects/Builder"], function(Resources, Object, ModelBuilder) {
    var manager = function() {
        this.resources = null;
        this.modelBuilder = new ModelBuilder(this);
        this.objects = {};
    }

    manager.prototype.setResources = function(res) {
        this.resources = res;
    }

    manager.prototype.setMap = function(map) {
        this.map = map;
    }

    manager.prototype.createObject = function (specs) {
        //console.log(specs);
        if (!this.resources) return false;

        if (specs.type == "v" || specs.type == "vehicle") {
            var that = this;

            var callback = function(model) {
                var object = new Object(model);
                object.id = specs.id;
                that.objects[specs.id] = object;
                object.x = specs.x;
                object.y = specs.y;

                that.map.add(object);
                object.x = null;
                object.y = null;
                object.applySpec(specs);
                if (specs.boundingBox)
                    object.addBoundingBox(specs.boundingBox);
            }

            this.modelBuilder.createVehicle(specs, callback);
            return;
        }

        var object = new Object(this.createModelBySpecs(specs));

        if (specs.radius) {
            object.radius = specs.radius;
        }

        object.id = specs.id;
        this.objects[specs.id] = object;
        object.x = specs.x;
        object.y = specs.y;

        this.map.add(object);
        object.x = null;
        object.y = null;

        object.applySpec(specs);
        if (specs.boundary) {
            if (specs.boundary.type=="circle") {
                object.addBoundaryCircle(specs.boundary);
            } else {
                object.addBoundaryPoints(specs.boundary);
            }
            object.boundary.type = specs.boundary.type;
        }
        if (specs.boundingBox) {
            object.addBoundingBox(specs.boundingBox);
        }

        return object;
    }

    manager.prototype.createRoad = function (specs) {
        var model;
        if (specs.type == "intersection"){
            model =  this.modelBuilder.createIntersection(specs);
        } else {
            model =  this.modelBuilder.createRoad(specs);
        }
        model.position.x = specs.x;
        model.position.y = specs.y;
        if (specs.rotation) {
            model.rotation.z = specs.rotation;
        }
        return model;
    }

    manager.prototype.createModelBySpecs = function (specs) {

        if (specs.type == "proj") {
            return this.modelBuilder.createProjectile(specs);
        }
        if (specs.type == "p") {
            return this.modelBuilder.createPerson(specs);
        }
        if (specs.type == "b") {
            return this.modelBuilder.createBuilding(specs);
        }
        if(specs.type == "v") {
            return this.modelBuilder.createVehicle(specs);
        }
        if (specs.type == "w") {
            return this.modelBuilder.createWeapon(specs);
        }
        if (specs.type == "e") {
            return this.modelBuilder.createExplosion(specs);
        }

        //console.log("Trying to build for unkknow type: " + specs.type);
    }

    manager.prototype.remove = function(object) {
        manageRemoveCount++;
        this.objects[object.id] = undefined;
        delete this.objects[object.id];
    }

    manager.prototype.getObject = function (id) {
        return this.objects[id];
    }

    manager.prototype.process = function (message) {
        if (message.type != "object") alert("wrong message got to objectManager: " + message.type);

        if (message.subType == "update") {
            this.applyUpdateOnObject(this.getObject(message.specs.id));
        }
        if (message.subType == "create") {
            return this.createObject(message.specs);
        }

        if (message.subType == "batch_create") {
            for (var i = 0; i < message.objects.length; i++) {
                this.createObject(message.objects[i]);
            }
        }

        if (message.subType == "batch_update") {
            for (var i = 0; i < message.objects.length; i ++ ) {
                var object = this.objects[message.objects[i].id];
                object.updateSpec = message.objects[i]
            }
        }

        if (message.subType == "remove") {
            //console.log(message);
            //removeMessageCount++;
            var object = this.getObject(message.id);
            if (object) {
                object.remove();
            }
            //object.markObjectForRemoval();
        }
    }

    return manager;
});
