define([
    "game/core/object/worker/object/Person",
    "game/core/object/worker/object/Vehicle",
    "game/core/object/worker/object/Building",
    "game/core/object/worker/object/Projectile",
    "game/core/object/worker/object/Explosion",
    "game/core/object/worker/object/projectile/Flame",
    "game/core/object/worker/object/projectile/Rocket",
    "game/core/object/worker/object/weapon/Minigun",
    "game/core/object/worker/object/weapon/Flamethrower",
    "game/core/object/worker/object/weapon/RocketLauncher"
], function(Person, Vehicle, Building, Projectile, Explosion, Flame, Rocket, Minigun, Flamethrower, RocketLauncher) {
    var manager = function() {

        this.container = {};
        this.pendingUpdates = [];
    }

    manager.prototype.addObject = function(key, value) {
        this.container[key] = value;

        return value;
    }

    manager.prototype.createObjects = function(specs) {
        var result = [];
        for (var i = 0; i < specs.length; i ++) {
            result.push(this.createObject(specs[i]));
        }

        messageManager.postObjects(result);

        return result;
    }


    manager.prototype.createObject = function(specs) {
        var object;
        if (specs.type == "person" || specs.type == "p") {
            object = this.createPerson(specs);
        } else if (specs.type == "vehicle" || specs.type == "v") {
            object = this.createVehicle(specs);
        } else if (specs.type == "building" || specs.type == "b") {
            object = this.createBuilding(specs);
        } else if (specs.type == "projectile" || specs.type == "proj") {
            object = this.createProjectile(specs);
        } else if (specs.type == "w") {
            object = this.createWeapon(specs);
        } else if (specs.type == "e") {
            object = this.createExplosion(specs);
        }
        this.addObject(specs.id, object);
        //postMessage(specs);
        return object;
    }

    manager.prototype.createPlayerObject = function(callback) {
        var that = this;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/GTA_project/player.json", false);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var specs = JSON.parse(xhr.responseText);
                    var object = that.createObject(specs);
                    callback(object);
                }
            }
        };
        xhr.send();
    }

    manager.prototype.createVehicle = function(specs) {
        return new Vehicle(specs);
    }

    manager.prototype.createPerson = function(specs) {
        return new Person(specs);
    }

    manager.prototype.createBuilding = function(specs) {
        return new Building(specs);
    }

    manager.prototype.createWeapon = function (specs) {
        var weapon = null;
        if (specs.subType == "minigun") { weapon = new Minigun(specs) }
        if (specs.subType == "flameThrower") { weapon = new Flamethrower(specs);}
        if (specs.subType == "rocketLauncher") { weapon = new RocketLauncher(specs);}

        return weapon;
    }

    manager.prototype.createProjectile = function (specs) {
        var projectile;
        if (specs.subType == "bullet") {projectile = new Projectile(specs);}
        if (specs.subType == "flame") {projectile = new Flame(specs);}
        if (specs.subType == "rocket") {projectile = new Rocket(specs);}

        return projectile;
    }

    manager.prototype.createExplosion = function(specs) {
        return new Explosion(specs);
    }

    manager.prototype.loadObjects = function(callback) {
        var that = this;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "../../data.json", false);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var specs = JSON.parse(xhr.responseText);
                    var objects = that.createObjects(specs);
                    callback(objects);
                }
            }
        };
        xhr.send();
    }

    manager.prototype.objectsToSpec = function (objects) {
        var result = [];
        for (var i = 0; i < objects.length; i++) {
            if (this.container[objects[i].id]) {
                result.push(objects[i].toSpec());
            }
        }

        return result;
    }

    manager.prototype.addUpdated = function (object) {
        this.pendingUpdates.push(object);
    }

    manager.prototype.clearUpdates = function() {
        this.pendingUpdates = []; // TODO: Might need optimization.
    }

    manager.prototype.update = function(dt) {
        if (!segmentManager.lattice) return;
        var segment = segmentManager.lattice[3][3];
        var count = 0;

        for (var key in this.container) {
            if (!this.container.hasOwnProperty(key)) continue;
            var object = this.container[key];
            object.update(this, dt);
            count++;
            if (Math.abs(parseInt(segment.x - object.x/segmentManager.multiplier)) > 4 || Math.abs(parseInt(segment.y - object.y/segmentManager.multiplier)) > 4) {
                //console.log(object.x + " : " + object.y);
                //console.log(Math.abs(segment.x - object.x/segmentManager.multiplier) + " : " + Math.abs(segment.y - object.y/segmentManager.multiplier));
                //object.markObjectForRemoval();
                this.removeObject(object);
            }

            /*if (Math.abs((player.object.x - object.x)/1000) > 6 || Math.abs((player.object.y - this.y)/1000) > 6) {
                this.markObjectForRemoval();
            }*/
        }
        //console.log('nr. of elements on worker: ' + count);
    }

    manager.prototype.removeObject = function (object) {
        //console.log("Asdasdasdasdasd");
        //console.log(object.id);
        //console.log("Asdasdasdasdasd");
        postMessage({"type": "object", subType: "remove",  "id": object.id});
        this.container[object.id] = null;
        delete this.container[object.id];
    }

    return manager;
});
