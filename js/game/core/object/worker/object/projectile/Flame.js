define([
    "game/core/object/worker/object/Projectile"
], function (Projectile) {
    var _super = Projectile.prototype;
    var p = function (specs) {
        Projectile.call(this,specs);
        this.type = "proj";
        this.subType = "flame";
        this.maxUpdateCount = 30;
        this.boundary = this.createCircleBoundary({boundary:{radius:10}});
    }

    p.prototype = Object.create(_super);

    p.prototype.update = function (manager) {
        if (this.updateCount <= this.maxUpdateCount) {
            if (this.updates.x != 0 || this.updates.y != 0 || this.updates.rotation != this.rotation ) {
                this.x += this.updates.x;
                this.y += this.updates.y;
                this.rotation = this.updates.rotation;
                this.updateBoundary();
                manager.addUpdated(this);
            }
            this.updateCount++;
        } else {
            manager.removeObject(this);
        }
    }

    p.prototype.updateBoundary = function () {
        this.boundary.center.x = this.x;
        this.boundary.center.y = this.y;
    }

    return p;
})

