define([
    "game/core/object/worker/object/Projectile"
], function (Projectile) {
    var _super = Projectile.prototype;
    var p = function (specs) {
        Projectile.call(this,specs);
        this.subType = "rocket";
        this.maxUpdateCount = 150;
        //this.boundary = this.createHistoryBoundary();
    }

    p.prototype = Object.create(_super);

    p.prototype.update = function (manager) {
        if (this.updateCount <= this.maxUpdateCount) {
            if (this.updates.x != 0 || this.updates.y != 0 || this.updates.rotation != this.rotation ) {
                this.boundary.points[0].x = this.x;
                this.boundary.points[0].y = this.y;
                this.x += this.updates.x;
                this.y += this.updates.y;
                this.rotation = this.updates.rotation;
                this.updateBoundary();
                manager.addUpdated(this);
            }
            this.updateCount++;
        } else {
            manager.removeObject(this);
        }
    }

    p.prototype.onCollision = function (object) {
        this.collisionChanges.remove = true;
        this.collisionChanges.generateExplosion = object.type != 'e';
    }

    p.prototype.applyCollisionChanges = function (manager) {

        if (this.collisionChanges.remove) {
            manager.removeObject(this);
        }

        if (this.collisionChanges.generateExplosion) {
            messageManager.postObject(manager.addObject(this.id + "_e", manager.createExplosion({
                id: this.id + "_e",
                x: this.x,
                y: this.y,
                radius: 10
            })));
        }
    }

    return p;
})