define([
    "game/core/object/worker/object/Object"
], function(AbstractObject) {
    var _super = AbstractObject.prototype;
    var p = function(specs) {
        AbstractObject.call(this, specs);
        this.type = "b";
        this.setupBoundary(specs);
    }
    p.prototype = Object.create(_super);

    return p;
});

