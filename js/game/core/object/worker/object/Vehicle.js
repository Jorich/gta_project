define([
    "game/core/object/worker/object/Object"
], function(AbstractObject) {
    var _super = AbstractObject.prototype;
    var v = function(specs) {
        AbstractObject.call(this, specs);
        // Physics state. Note that vel is also a vector although
        // currently only engine effects are accounted for, there's no
        // lateral component.
        this.state = { "pos": new THREE.Vector2(0, 0),
                       "vel": new THREE.Vector2(0, 0)
                     };

        // Engine force driving the wheels. A better option would
        // probably be to have a "gas" variable and calculate the
        // force from the engine torque and so on. Note that this
        // force brakes when it's negative.
        this.engineForce = 0;
        this.engineForceMax = 500;
        this.engineForceMaxReverse = -200;

        // Steering angle
        this.steeringAngle = 0;
        this.steeringAngleMax = 0.8;
        this.steeringAngleStep = 0.03;
        this.selfAlignment = 0.03;

        this.wheelbase = 80;

        // Coefficient of drag
        this.drag = 1;

        this.bumpDampening = 0.2;

        // Inherited variables
        this.type = "v";

        if (specs.colour)
            this.colour = specs.colour;
        else
            this.colour = [0.3, 0.3, 0.3];

        this.setupBoundary(specs);

        this.enterTime = 0;
    }

    v.prototype = Object.create(_super);

    v.prototype.setX = function(x) {
        _super.setY.call(this, x);
        this.state.pos.x = x;
    }

    v.prototype.setY = function(y) {
        _super.setY.call(this, y);
        this.state.pos.y = y;
    }

    v.prototype.update = function(manager, dt) {
        // Reduce steering angle to simulate self aligning torque.
        if (this.steeringAngle > 0)
            this.steeringAngle = Math.max(0, this.steeringAngle - this.selfAlignment);
        else if (this.steeringAngle < 0)
            this.steeringAngle = Math.min(0, this.steeringAngle + this.selfAlignment);

        this.integrate(dt);
        // Update object position from the physics state
        _super.update.call(this, manager);
    }

    // This is the core of physics simulation. Add all forces here and
    // calculate acceleration from the resulting force.
    v.prototype.acceleration = function(state) {
        // Calculate acceleration from the total force
        var force = new THREE.Vector2(0, 0);
        force.y += this.engineForce;
        force.y += (- this.drag) * state.vel.y;

        // We should divide by mass here but this is simple game
        // physics so we just return force
        return force;
    }

    v.prototype.evaluate = function(dt, derivative) {
        if (typeof derivative === 'undefined' ||
            typeof dt === 'undefined')
        {
            // In this case we use the current state and do not step
            // forward. Return derivatives at current time.

            return { "vel": this.state.vel,
                     "acc": this.acceleration(this.state)
                   };
        } else {
            // Do Euler step and return derivatives at current time.

            var pos = this.state.pos.clone()
                .add(derivative.vel.clone().multiplyScalar(dt));
            var vel = this.state.vel.clone()
                .add(derivative.acc.clone().multiplyScalar(dt));

            var state = { "pos": pos,
                          "vel": vel
                        };

            return { "vel": vel,
                     "acc": this.acceleration(state)
                   };
        }
    }

    // Runge-Kutta 4 integration.
    // http://gafferongames.com/game-physics/integration-basics/
    v.prototype.integrate = function(dt) {
        var a = this.evaluate();
        var b = this.evaluate(dt * 0.5, a);
        var c = this.evaluate(dt * 0.5, b);
        var d = this.evaluate(dt, c);

        var vel = b.vel.clone().add(c.vel).multiplyScalar(2)
            .add(a.vel).add(d.vel).multiplyScalar(1/6);
        var acc = b.acc.clone().add(c.acc).multiplyScalar(2)
            .add(a.acc).add(d.acc).multiplyScalar(1/6);

        // Reduce steering angle at high speeds to simulate understeer.
        var steeringAngle = this.steeringAngle * (1 - this.state.vel.y / 700);

        var radius = this.wheelbase / Math.sin(steeringAngle);
        // How much the car turns. vel.y / radius is the angular
        // velocity.
        var angle = vel.y / radius * dt;

        if (Math.abs(angle) < 0.000001) {
            // The angle is negligible, move in a straight line in the
            // direction the car is pointing
            var x = rotate([0, vel.y * dt], -this.rotation);
            this.state.pos.x += x[0];
            this.state.pos.y += x[1];
            this.updates.x = x[0];
            this.updates.y = x[1];
        } else {
            // Calculate the position of the front, rear and middle in
            // a coordinate system where the car is pointing upwards
            // and has been translated to the right by the radius of
            // the turning circle. Note that the rear of the car is on
            // the x axis because a car rotates around the mid-point
            // of the rear axis. We rotate the points in this space as
            // the car moves along the circle and then calculate the
            // resulting rotation of the car and how much the midpoint
            // moved (translation). This whole thing can probably be
            // simplified if you write out the complete equations for
            // rotation and translation. You can also reduce the
            // number of list objects created.
            var rear = [radius, 0];
            var front = [radius, this.wheelbase];
            var middle = [radius, this.wheelbase / 2];
            var rearNew = rotate(rear, angle);
            var frontNew = rotate(front, angle);
            var middleNew = rotate(middle, angle);

            // This calculates the angle of the line connecting the
            // front and rear. Since the car started out at a 90
            // degree angle, we subtract 90 degrees to find out how
            // much the rotation changed.
            var rotation = Math.atan2(frontNew[1] - rearNew[1], frontNew[0] - rearNew[0]) - 1.57079633;

            var translation = [middleNew[0] - middle[0], middleNew[1] - middle[1]];
            translation = rotate(translation, -this.rotation);

            // The coordinate space we used is different and we have to
            // subtract.
            //this.rotation -= rotation;
            this.updates.rotation = this.rotation - rotation;
            this.state.pos.x -= translation[0];
            this.state.pos.y -= translation[1];
            this.updates.x =  - translation[0];
            this.updates.y =  - translation[1];
        }

        this.state.vel.add(acc.multiplyScalar(dt));
    }
    v.prototype.increaseEngineForce = function() {
        this.engineForce = this.engineForceMax;
    }

    v.prototype.decreaseEngineForce = function() {
        this.engineForce = this.engineForceMaxReverse;
    }

    v.prototype.killEngineForce = function() {
        this.engineForce = 0;
    }

    v.prototype.increaseSteeringAngle = function() {
        this.steeringAngle = clamp(this.steeringAngle + this.steeringAngleStep,
                                   -this.steeringAngleMax,
                                   this.steeringAngleMax);
    }

    v.prototype.decreaseSteeringAngle = function() {
        this.steeringAngle = clamp(this.steeringAngle - this.steeringAngleStep,
                                   -this.steeringAngleMax,
                                   this.steeringAngleMax);
    }

    var clamp = function(x, min, max) {
        return Math.max(Math.min(x, max), min);
    }

    var rotate = function(vec, angle) {
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);
        return [cos * vec[0] + sin * vec[1],
                -sin * vec[0] + cos * vec[1]];
    }

    v.prototype.onCollision = function (object) {
        if (object.type == "b" ||
            (object.type == "v" && Math.abs(this.state.vel.y) > 0.00001))
        {
            var mult = (this.state.vel.y < 0) ? -1 : 1;
            // We need penetration depth. Hack some kind of estimation.
            var depth = this.state.vel.y * 0.025 + 20;
            var displace = rotate([0, -depth * mult], -this.rotation);

            this.state.pos.x += displace[0];
            this.state.pos.y += displace[1];
            this.updates.x = displace[0];
            this.updates.y = displace[1];
            this.state.vel.y = this.state.vel.y * (-this.bumpDampening);
        }
    }

    v.prototype.applyCollisionChanges = function (manager) {
        this.x += this.updates.x;
        this.y += this.updates.y;
        this.updateBoundary();
    }

    v.prototype.toSpec = function () {
        var spec = _super.toSpec.call(this);
        spec.colour = this.colour;
        return spec;
    }

    return v;
});
