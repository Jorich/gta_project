define([
    "game/core/collision/Boundary",
    "game/core/collision/boundary/CircleBoundary",
    "game/core/collision/Vertex",
    "game/core/collision/AABoundingBox"
], function(Boundary, CircleBoundary, Vertex, AABoundingBox) {
    var o = function (spec) {
        this.id = spec.id;
        this.x = spec.x;
        this.y = spec.y;
        this.rotation = spec.rotation ? spec.rotation : 0;

        this.updates = {
            x: 0,
            y: 0,
            rotation: 0
        }
        this.revert = {
            rotation: 0
        }

        this.remove = false;
        
        if(spec.state) {
        	this.state = spec.state;
        }

        this.inWorld = true;
    }

    o.prototype.setupBoundary = function (spec) {
        this.boundary = this.createBoundaryBySpec(spec);
        this.boundingBox = this.createBoundingBox(this.x, this.y, spec.boundingBox.width, spec.boundingBox.height)
    }

    o.prototype.createBoundingBox = function(x, y, w, h) {
        return new AABoundingBox(x, y, w, h);
    }

    o.prototype.createBoundaryBySpec = function (spec) {
        if (spec.boundary.type == "circle") {
            return this.createCircleBoundary(spec);
        }
        if (spec.boundary.type == "polygon") {
            return this.createPolygonBoundary(spec);
        }
        console.log("unknown boundary:" + JSON.stringify(spec));
    }

    o.prototype.createCircleBoundary = function(spec) {
        return new CircleBoundary(new Vertex(this, this.x, this.y), spec.boundary.radius);
    }

    o.prototype.createPolygonBoundary = function(spec) {
        return new Boundary(this.createBoundaryPoints(spec.boundary.points,  this.x, this.y));
    }

    o.prototype.setX = function(x) {
        this.x = x;
        if (this.boundary) {
            this.boundary.setX(x);
        }
        if (this.boundingBox) {
            this.boundingBox.center.x = x;
        }
    }

    o.prototype.setY = function(y) {
        this.y = y;
        if (this.boundary) {
            this.boundary.setY(y);
        }
        if (this.boundingBox) {
            this.boundingBox.center.y = y;
        }
    }

    o.prototype.getBoundingBox = function () {
        this.boundingBox.center.x = this.x;
        this.boundingBox.center.y = this.y;

        return this.boundingBox;
    }

    o.prototype.getBoundary = function () {
        return this.boundary;
    }

    o.prototype.update = function(manager) {
        if (this.remove) {
            //console.log("REMOVING ITEM!!! ");
            manager.removeObject(this);
            return;
        }

        if (this.updates.x != 0 || this.updates.y != 0 || this.updates.rotation != this.rotation ) {
            this.x += this.updates.x;
            this.y += this.updates.y;
            this.rotation = (this.updates.rotation) % (2 * Math.PI);
            this.updateBoundary();
            manager.addUpdated(this);
        }
    }

    o.prototype.revertUpdate = function () {

        this.updates.x = -this.updates.x;
        this.updates.y = -this.updates.y;
        var tempRot = this.updates.rotation;
        this.updates.rotation = this.revert.rotation;

        if (this.updates.x != 0 || this.updates.y != 0 || this.updates.rotation != this.rotation ) {
            this.x += this.updates.x;
            this.y += this.updates.y;
            this.rotation = (this.updates.rotation) % (2 * Math.PI);
            this.updateBoundary();
        }
        this.updates.x = -this.updates.x;
        this.updates.y = -this.updates.y;
        this.updates.rotation = tempRot;
    }

    o.prototype.updateBoundary = function () {

        if (this.boundary && this.boundary.type == "polygon") {
            this.boundary.x = this.x;
            this.boundary.y = this.y;
            this.boundary.update(this.updates);

        }
        if (this.boundary.type == "circle") {
            this.boundary.center.x = this.x;
            this.boundary.center.y = this.y;
        }
    }

    o.prototype.createBoundaryPoints = function (points) {
        var result = [];
        for (var i = 0; i < points.length; i++) {
            var point = points[i];
            result.push(new Vertex(this, this.x + point.x, this.y + point.y));
        }
        return result;
    }

    o.prototype.toSpec = function () {
        var specs = {
            id: this.id,
            x: this.x,
            y: this.y,
            rotation: this.rotation,
            type: this.type
        };
        /*if (this.boundary) {
            specs.boundary = this.boundary.toSpec();
        }
        if (this.boundingBox) {
            specs.boundingBox = {
                x: this.boundingBox.center.x,
                y: this.boundingBox.center.y,
                h: this.boundingBox.halfDimensions.y * 2,
                w: this.boundingBox.halfDimensions.x * 2
            }
        }*/
        if(this.subType) {
        	specs.subType = this.subType;
        }
        if(this.updateCount) {
        	specs.updateCount = this.updateCount;
        }
        if(this.state) {
        	specs.state = this.state;
        }

        return specs;
    }
    o.prototype.markObjectForRemoval = function () {
        this.remove = true;
    }

    o.prototype.onCollision = function (object) {}
    o.prototype.applyCollisionChanges = function (manager) {}

    return o;
});
