define([
    "game/core/object/worker/object/Object",
    "game/core/collision/Vertex",
    "game/core/collision/Boundary"
], function (AbstractObject, Vertex, Boundary) {
    var _super = AbstractObject.prototype;
    var p = function (specs) {
        AbstractObject.call(this,specs);
        this.type = "proj";
        this.subType = "bullet";
        this.maxUpdateCount = 100;
        this.updateCount = 0;
        this.collisionChanges = {
            remove: false
        }
        this.setDirection(specs.direction);
        this.createHistoryBoundary();
    }

    p.prototype = Object.create(_super);

    p.prototype.setDirection = function (direction) {
        this.updates.x = direction.x;
        this.updates.y = direction.y;
    }

    p.prototype.createHistoryBoundary = function() {
        this.boundary = new Boundary([new Vertex(this, this.x, this.y),new Vertex(this, this.x, this.y)], this.x, this.y);
    }

    p.prototype.update = function (manager) {
        if (this.updateCount <= this.maxUpdateCount && !(this.remove)) {
            this.boundary.points[0].x = this.x;
            this.boundary.points[0].y = this.y;
            _super.update.call(this, manager);
            this.updateCount++;
        } else {
            manager.removeObject(this);
        }

    }

    p.prototype.updateBoundary = function () {
        this.boundary.points[1].x = this.x;
        this.boundary.points[1].y = this.y;
        this.boundary.x = this.x;
        this.boundary.y = this.y;
    }

    p.prototype.getBoundingBox = function () { return null;}

    p.prototype.onCollision = function (object) {
        this.collisionChanges.remove = true;
    }

    p.prototype.applyCollisionChanges = function (manager) {
        if (this.collisionChanges.remove) {
            manager.removeObject(this);
        }
    }

    return p;
})
