define([
    "game/core/collision/Boundary",
    "game/core/collision/Vertex",
    "game/core/collision/AABoundingBox"
], function(Boundary, Vertex, AABoundingBox) {

    var p = function(id) {
        this.id = id;
        this.x = 0;
        this.y = 0;
        this.rotation = 0;
        this.type = "v";

        this.updates = {
            x: 0,
            y: 0,
            rotation: 0
        }
        this.boundary = new Boundary (this.createBoundaryPoints());
        this.boundingBox = new AABoundingBox(this.x, this.y, 25,50);
    }
    p.prototype.setX = function(x) {
        this.x = x;
    }

    p.prototype.setY = function(y) {
        this.y = y;
    }

    p.prototype.update = function(manager) {

        if (this.updates.x != 0 || this.updates.y != 0 || this.updates.rotation != this.rotation ) {

            this.x += this.updates.x;
            this.y += this.updates.y;
            this.rotation = this.updates.rotation;

            this.boundary.update(this.updates);

            if (manager) {
                manager.addUpdated(this);
            }
        }
    }

    p.prototype.revertUpdate = function () {
        if (this.updates.x != 0 || this.updates.y != 0 || this.updates.rotation != this.rotation ) {
            this.updates.x = -this.updates.x;
            this.updates.y = -this.updates.y;
            this.x += this.updates.x;
            this.y += this.updates.y;
            this.boundary.update(this.updates);

            this.updates.x = -this.updates.x;
            this.updates.y = -this.updates.y;
        }
    }

    p.prototype.setX = function(x) {
        this.x = x;
        if (this.boundary) {
            this.boundary.setX(x);
        }
        if (this.boundingBox) {
            this.boundingBox.center.x = x;
        }

    }

    p.prototype.setY = function(y) {
        this.y = y;
        if (this.boundary) {
            this.boundary.setY(y);
        }
        if (this.boundingBox) {
            this.boundingBox.center.y = y;
        }
    }

    p.prototype.createBoundaryPoints = function () {

        var upperLeft = new Vertex(this, this.x - 25, this.y + 50);
        var upperRight = new Vertex(this, this.x + 25, this.y + 50);
        var lowerLeft = new Vertex(this, this.x - 25, this.y - 50);
        var lowerRight = new Vertex(this, this.x + 25, this.y - 50);

        return [upperLeft, upperRight, lowerRight, lowerLeft];
    }


    p.prototype.getBoundingBox = function () {
        this.boundingBox.center.x = this.x;
        this.boundingBox.center.y = this.y;

        return this.boundingBox;
    }

    return p;
});

