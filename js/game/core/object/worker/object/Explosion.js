define([
    "game/core/object/worker/object/Object"
], function(AbstractObject) {
    var _super = AbstractObject.prototype;
    var p = function(specs) {
        AbstractObject.call(this, specs);
        this.type = "e";

        this.maxUpdateCount = 50;
        this.updateCount = 0;
        this.radius = specs.radius;
        this.increment = 1.05;

        this.boundary = this.createCircleBoundary({boundary:{radius:this.radius}});
        this.boundingBox = this.createBoundingBox(this.x, this.y, this.radius, this.radius);
    }
    p.prototype = Object.create(_super);

    p.prototype.update = function (manager) {
        if (this.updateCount <= this.maxUpdateCount) {
            this.radius = this.increment * this.radius;
            if (this.boundary && this.boundary.type == 'circle') {
                this.boundary.radius = this.radius;
                this.boundingBox.halfDimensions.x = this.radius;
                this.boundingBox.halfDimensions.y = this.radius;
            }
            this.updateBoundary();
            this.updateCount++;
            manager.addUpdated(this);
        } else {
            manager.removeObject(this);
        }
    }

    p.prototype.toSpec = function () {
        var spec = _super.toSpec.call(this);
        spec.radius = this.radius;
        return spec;
    }

    return p;
});

