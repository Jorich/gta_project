define(['game/core/object/worker/object/Object'], function (AbstractObject) {
    var _super = AbstractObject.prototype;
    var minigun = function (specs) {
        AbstractObject.call(this,specs);
        this.type = "w";
        this.subType = "mg";
        this.bulletCount = 500;
        this.shotsDone = 0;

        this.sps = 100;
        this.then = Date.now();
        this.interval = 1000 / this.sps;
        this.delta = 0;

        this.collisionChanges = {
            remove: false
        }
        this.setupBoundary(specs);
    }
    minigun.prototype = Object.create(_super);

    minigun.prototype.shoot = function (direction, manager) {
        this.now = Date.now();
        this.delta = this.now - this.then;
        if (this.delta > this.interval) {
            this.shotsDone +=1;
            messageManager.postObject(manager.addObject(this.id + "_" + this.shotsDone, manager.createProjectile({
                id: this.id + "_" + this.shotsDone,
                x: this.x + 4* direction.x,
                y: this.y + 4* direction.y,
                direction: direction,
                subType: "bullet"
            })));

            this.then = this.now - (this.delta % this.interval);
        }
    }

    minigun.prototype.onCollision = function (object) {
        this.collisionChanges.remove = object.type == "p";
    }

    minigun.prototype.applyCollisionChanges = function (manager) {
        if (this.collisionChanges.remove) {
            manager.removeObject(this);
        }
    }

    minigun.prototype.random = function(min, max) {
        return Math.random() * (max - min) + min;
    }

    return minigun;
});
