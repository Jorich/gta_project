define(['game/core/object/worker/object/weapon/Minigun'], function (Minigun) {
    var _super = Minigun.prototype;
    var launcher = function (specs) {
        Minigun.call(this,specs);
        this.subType = "rl";
        this.sps = 3;
        this.interval = 1000 / this.sps;
        this.setupBoundary(specs);
    }
    launcher.prototype = Object.create(_super);

    launcher.prototype.shoot = function (direction, manager) {
        this.now = Date.now();
        this.delta = this.now - this.then;
        if (this.delta > this.interval) {
            this.shotsDone +=1;
            messageManager.postObject(manager.addObject(this.id + "_" + this.shotsDone, manager.createProjectile({
                id: this.id + "_" + this.shotsDone,
                x: this.x + 4* direction.x,
                y: this.y + 4* direction.y,
                direction: direction,
                subType: "rocket"
            })));

            this.then = this.now - (this.delta % this.interval);
        }
    }

    launcher.prototype.applyCollisionChanges = function (manager) {
        if (this.collisionChanges.remove) {
            manager.removeObject(this);
        }
    }

    launcher.prototype.random = function(min, max) {
        return Math.random() * (max - min) + min;
    }

    return launcher;
});
