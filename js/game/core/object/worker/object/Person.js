define([
    "game/core/object/worker/object/Object"
], function(AbstractObject) {
    var _super = AbstractObject.prototype;
    var p = function(specs) {
        AbstractObject.call(this, specs);
        this.type = "p";
        this.setupBoundary(specs);
        this.ePressed = false;
    }
    p.prototype = Object.create(_super);

    p.prototype.onCollision = function (object) {

        if (object.type == "w" && this.id == player.object.id) {
            player.setWeapon(object);
        } else if (object.type == "v") {
            if (this.ePressed) {
                objectManager.removeObject(this);
                postMessage({type: "initPlayer", subType: "update", specs: object.toSpec()});
                player.setObject(object);
                controls.setObject(object);
                object.enterTime = new Date().getTime();
            }
            this.revertUpdates = true;
        } else {
            if (object.type != "proj" && object.type != 'e') {this.revertUpdates = true;}
        }
    }

    p.prototype.applyCollisionChanges = function (manager) {
        if (this.revertUpdates) {
            this.revertUpdate();
            this.revertUpdates = false;
        }
    }

    p.prototype.update = function (manager) {
        _super.update.call(this, manager);
        if (player.weapon != null) {
            player.weapon.x =this.x;
            player.weapon.y =this.y;
        }
    }
/*
    p.prototype.handleObjectsInProximity = function (objects) {
        if (player.controls.mouseButtonPressed) {
            var closestVehicle;
            for (var i in objects) {
                if (objects)
            }
        }
    }
*/
    return p;
});
