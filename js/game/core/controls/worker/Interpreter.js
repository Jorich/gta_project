define([
    'game/core/controls/worker/interpreters/PersonMovementInterpreter',
    'game/core/controls/worker/interpreters/VehicleMovementInterpreter'
], function(PersonInterpreter, VehicleInterpreter) {
    var interpreter = function(objectManager) {
        this.currentInterpreter = null;
        this.personInterpreter = new PersonInterpreter(objectManager);
        this.vehicleInterpreter = new VehicleInterpreter(objectManager);
        this.object = null;
        this.player = null;
    }

    interpreter.prototype.process = function(message) {
        if (!this.currentInterpreter) {
            console.log('Interpreter is not set');
        }
        //console.log(this.object);
        this.currentInterpreter.process(message, this.object);
    }

    interpreter.prototype.setPlayer = function(player) {
        this.player = player;
    }

    interpreter.prototype.setInterpreterByObject = function (object) {
        if (object.type == "person" || object.type == "p") {
            this.currentInterpreter = this.personInterpreter;
        }
        if (object.type == "vehicle" || object.type == "v") {
            this.currentInterpreter = this.vehicleInterpreter;
        }
    }

    interpreter.prototype.setObject = function (object) {
        this.object = object;
        this.setInterpreterByObject(object);
    }

    return interpreter;
});
