define([], function() {

    var interpreter = function(objectManager) {
        this.objectManager = objectManager;
    }

    interpreter.prototype.process = function(message, object) {
        var b = message.body;

        if (b.upPressed) {
            object.increaseEngineForce();
        } else if (b.downPressed) {
            object.decreaseEngineForce();
        } else {
            object.killEngineForce();
        }

        if (b.leftPressed) {
            object.decreaseSteeringAngle();
        }
        if (b.rightPressed) {
            object.increaseSteeringAngle();
        }

        if (b.ePressed) {
            if (new Date().getTime() - object.enterTime > 1000) {
                var angle = object.rotation;
                var cos = Math.cos(-angle);
                var sin = Math.sin(-angle);
                var vec = [-40, 0];
                var x = object.x + (cos * vec[0] + sin * vec[1]);
                var y = object.y + (-sin * vec[0] + cos * vec[1]);

                var specs = { "id": "player_1",
                              "type": "p",
                              "state": "idle",
                              "x": x,
                              "y": y,
                              "boundary": {
                                  "type": "polygon",
                                  "points": [
                                      {"x":-10, "y":5},
                                      {"x":10, "y":5},
                                      {"x":-10, "y":-5},
                                      {"x":10, "y":-5}
                                  ]
                              },
                              "boundingBox": {
                                  "width": 10,
                                  "height": 5
                              }
                            };
                var person = objectManager.createObject(specs);
                var spec2 = person.toSpec();
                postMessage({type: "object", subType: "create", specs: spec2});
                postMessage({type: "initPlayer", subType: "update", specs: spec2});
                player.setObject(person);
                controls.setObject(person);
            }
        }
    }

    return interpreter;
});
