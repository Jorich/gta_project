define([], function() {
    var interpreter = function(objectManager) {
        this.objectManager = objectManager;
        this.speed = 5;
        this.usePressed = false;
    }

    interpreter.prototype.process = function(message, object) {
        var b = message.body;
        object.updates.rotation = b.rotation;

        if(b.upPressed || b.downPressed || b.leftPressed || b.rightPressed) {
        	object.state = "moving";
        } else {
        	object.state = "idle";
        }

        if (b.upPressed) {
            object.updates.y = this.speed;
        }
        if (b.downPressed) {
            object.updates.y = - this.speed;
        }

        if (!b.downPressed && !b.upPressed) {
            object.updates.y = 0;
        }

        if (b.leftPressed) {
            object.updates.x = this.speed;
        }

        if (b.rightPressed) {
            object.updates.x = - this.speed;
        }

        if (!b.leftPressed && !b.rightPressed) {
            object.updates.x = 0;
        }

        object.ePressed = b.ePressed;

        this.usePressed = b.usePressed;
        
        if(b.swapWeapon) {
        	var w = player.weapons[b.swapWeapon - 1]
        	if(w) {
        		player.weapon = w; 
        	}
        }

        if (b.mouseButtonPressed){
        	//object.state += "shooting";
            if (player.weapon) {
                var direction = {
                    x: b.mouse.x - object.x,
                    y: b.mouse.y - object.y
                }
                var length = Math.sqrt(direction.x * direction.x + direction.y * direction.y);
                direction.x = 9* direction.x/length;
                direction.y = 9* direction.y/length;
                player.weapon.shoot(direction, this.objectManager);
            }
        }
    }

    return interpreter;
});
