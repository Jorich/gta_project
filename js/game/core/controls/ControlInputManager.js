define([], function() {
    var manager = function(worker, object) {
        this.worker = worker;
        this.object = object;
        this.messageType = "control";
        this.sceneContainer = $('canvas');

        this.messageBody = {
            upPressed : false,
            downPressed : false,
            rightPressed : false,
            leftPressed : false,
            ePressed: false,
            rotation: object.model.rotation.z,
            mouseButtonPressed : false,
            mouse: {
                x: 0,
                y: 0
            }
        }

    }

    manager.prototype.postMessage = function () {
        this.worker.postMessage({type: this.messageType, body: this.messageBody});
    }

    manager.prototype.init = function() {
        var self = this;
        window.addEventListener('keydown', function(event) {
            if (event.keyCode == 87) {
                self.messageBody.upPressed = true;
            }
            if (event.keyCode == 83) {
                self.messageBody.downPressed = true;
            }
            if (event.keyCode == 68) {
                self.messageBody.leftPressed = true;
            }
            if (event.keyCode == 65) {
                self.messageBody.rightPressed = true;
            }
            if (event.keyCode == 69) {
                self.messageBody.ePressed = true;
            }
            if (event.keyCode == 49) {
                self.messageBody.swapWeapon = 1;
                self.messageBody.mouseButtonPressed = false;
            }
            if (event.keyCode == 50) {
            	self.messageBody.swapWeapon = 2;
            	self.messageBody.mouseButtonPressed = false;
            }
            if (event.keyCode == 51) {
            	self.messageBody.swapWeapon = 3;
            	self.messageBody.mouseButtonPressed = false;
            }
        }, false);

        window.addEventListener('keyup', function(event) {
            if (event.keyCode == 87) {
                self.messageBody.upPressed = false;
            }
            if (event.keyCode == 83) {
                self.messageBody.downPressed = false;
            }
            if (event.keyCode == 68) {
                self.messageBody.leftPressed = false;
            }
            if (event.keyCode == 65) {
                self.messageBody.rightPressed = false;
            }
            if (event.keyCode == 69) {
                self.messageBody.ePressed = false;
            }
        }, false);

        window.addEventListener('mousedown', function(event) {
            self.messageBody.mouseButtonPressed = true;
        });

        window.addEventListener('mouseup', function(event) {
            self.messageBody.mouseButtonPressed = false;
        });

        self.raycaster = new THREE.Raycaster();

        self.mouse = new THREE.Vector2();
        $(document).mousemove(function(event){
            self.mouse.x = (event.clientX / self.sceneContainer.width()) * 2 - 1 ;
            self.mouse.y = - (event.clientY / self.sceneContainer.height()) * 2 + 1;
        });

    }

    manager.prototype.registerMousePosition = function () {
        this.raycaster.setFromCamera(this.mouse, game.camera);
        var intersection = this.raycaster.intersectObject(game.player.object.segment.floor);

        if (intersection.length > 0) {
            var point = intersection[0].point;
            this.messageBody.rotation = 1.5707 +  Math.atan2(this.object.y - point.y, this.object.x - point.x);
            this.messageBody.mouse.x = point.x;
            this.messageBody.mouse.y = point.y;
        } else {
            var neighbours = game.player.object.segment.neighbours;
            for (var i = 0; i < neighbours.length; i++) {
                intersection = this.raycaster.intersectObject(neighbours[i].floor);
                if (intersection.length > 0) {
                    var point = intersection[0].point;
                    this.messageBody.rotation = 1.5707 +  Math.atan2(this.object.y - point.y, this.object.x - point.x);
                    this.messageBody.mouse.x = point.x;
                    this.messageBody.mouse.y = point.y;
                }
            }
        }
    }

    manager.prototype.setObject = function (object) {
        this.object = object;
    }

    return manager;
});
