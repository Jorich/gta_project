define([], function () {
    var player = function() {
        this.object = null;
        this.weapon = null;
        this.weapons = [];
        this.controls = null;
    };

    player.prototype.setWeapon = function (weapon) {
    	switch(weapon.subType) {
        case 'mg':
            this.weapons[0] = weapon;
            break;
        case 'ft':
        	this.weapons[1] = weapon;
            break;
        case 'rl':
        	this.weapons[2] = weapon;
            break;
    	}
        this.weapon = weapon;
    }

    player.prototype.setObject = function (object) {
        this.object = object;
        if (this.controls) {
            this.controls.setObject(object);
        }
    }

    player.prototype.setControls = function (controls) {
        this.controls = controls;
    }

    return player;
});
