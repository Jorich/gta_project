define(["game/core/worker/Segment"], function(Segment) {
    var manager = function(messageManager, objectManager) {
        this.messageManager = messageManager;
        this.objectManager = objectManager;
        this.multiplier = 1000;
        this.dataLoading = false;
        this.lastUpdated = {
            x: 0,
            y: 0
        };

        this.debug = false;
    }

    manager.prototype.init = function() {
        this.loadInitialSegmentData();
    }

    manager.prototype.loadInitialSegmentData = function () {
        var that = this;
        var xhr = new XMLHttpRequest();
        var url = '';
        if (this.debug) {
            url = "/GTA_project/data.json";
        } else {
            url = "/GTA_project/init.php";
        }

        xhr.open("GET", url, false);
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var specs = JSON.parse(xhr.responseText);
                    that.createLattice(specs);
                }
            }
        };
        xhr.send();
    }

    manager.prototype.createLattice = function(data) {
        this.lattice = data;
        this.lattice.sort(this.rowComparator);

        for (var i = 0; i < this.lattice.length; i++) {
            this.lattice[i].sort(this.columnComparator);
            for (var j = 0 ; j < this.lattice[i].length; j++) {
                this.lattice[i][j] = this.processSegmentData(this.lattice[i][j]);
            }
        }
    }

    manager.prototype.columnComparator = function (a,b) {
        if (a.x < b.x)
            return -1;
        if (a.x > b.x)
            return 1;
        return 0;
    }

    manager.prototype.columnListComparator = function(a,b) {
        if (a.y < b.y)
            return 1;
        if (a.y > b.y)
            return -1;
        return 0;
    }

    manager.prototype.rowComparator = function (a,b) {
        if (a[0].y < b[0].y)
            return 1;
        if (a[0].y > b[0].y)
            return -1;
        return 0;
    }

    manager.prototype.processSegmentData = function (specs) {
        var segment = this.createSegment(specs);
        if (specs.player) {
            console.log(specs.player);
            messageManager.onPlayerLoaded(objectManager.createObject(specs.player));
        }
        if (specs.objects) {
            objectManager.createObjects(specs.objects);
        }

        return segment;
    }

    manager.prototype.createSegment = function(specs) {
        postMessage({type: "map", subType: "create", specs: specs});
        return new Segment(specs.x , specs.y);
    }

    manager.prototype.unloadRow = function(index) {
        if (index == 0) {

            for (var i = 0 ; i < this.lattice.length; i++) {
                var segment = this.lattice[6][i];
                postMessage({type: 'map', subType: 'remove', specs: {x: segment.x, y: segment.y}});
            }

            this.lattice.pop();
        } else if (index == 6) {
            for (var i = 0 ; i < this.lattice.length; i++) {
                var segment = this.lattice[0][i];
                postMessage({type: 'map', subType: 'remove', specs: {x: segment.x, y: segment.y}});
            }

            this.lattice.splice(0,1);
        } else {
            console.log("asdasdasdasdasd");
        }
    }

    manager.prototype.unloadColumn = function (index) {
        //this.print();
        for (var i = 0 ; i < this.lattice.length; i++) {

            if (index == 6) {
                postMessage({type: 'map', subType: 'remove', specs: {x: this.lattice[i][0].x, y: this.lattice[i][0].y}});
                this.lattice[i].splice(0,1);

            } else if (index == 0) {
                postMessage({type: 'map', subType: 'remove', specs: {x: this.lattice[i][6].x, y: this.lattice[i][6].y}});
                this.lattice[i].pop();

            } else {
                console.log("asdasdasdasdasd");
            }
        }
        /*console.log('AFTER');
        this.print();
        console.log('END');*/
    }

    manager.prototype.loadRow = function (index, oldy, y) {
        console.log("Loading Row");
        //console.log(this.lastUpdated);
        if (this.dataLoading) return;
        this.dataLoading = true;
        var that = this;
        var xhr = new XMLHttpRequest();
        var url = '';
        if (this.debug) {
            url = "/GTA_project/data.json"
        } else {
            url = "/GTA_project/simulator.php?command=row&x=" + this.lastUpdated.x + "&y="+ y
        }
        xhr.open("GET", url , false);
        //console.log("/GTA_project/simulator.php?command=row&x=" + this.lastUpdated.x + "&y="+ y);
        this.lastUpdated.y = oldy;
        xhr.onreadystatechange = function () {
            //console.log("Response for Row");
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var specs = JSON.parse(xhr.responseText);
                    //console.log(specs);
                    that.unloadRow(index);
                    /*console.log("after delete")
                    that.print();
                    console.log("after delete  asa ")*/
                    specs.sort(that.columnComparator);
                    if (index == 0) {
                        that.lattice.unshift(specs);
                    } else if (index == 6) {
                        that.lattice.push(specs);
                    } else {
                        console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                    }
                    for (var j = 0 ; j < specs.length; j++) {
                        that.lattice[index][j] = that.processSegmentData(that.lattice[index][j]);
                    }
                }
                that.dataLoading = false;
                /*console.log("After Row was over it");
                that.print();
                console.log("After Row was over it#######################");*/
            }
        };
        xhr.send();
    }

    manager.prototype.loadColumn = function (index, oldx, x) {
        console.log("Loading Column");
        //console.log(this.lastUpdated);
        if (this.dataLoading) return;
        this.dataLoading = true;
        var that = this;
        var xhr = new XMLHttpRequest();
        //console.log("/GTA_project/simulator.php?command=col&x=" + x + "&y="+ this.lastUpdated.y);
        xhr.open("GET", "/GTA_project/simulator.php?command=col&x=" + x + "&y="+ this.lastUpdated.y , false);
        this.lastUpdated.x = oldx;
        xhr.onreadystatechange = function () {
            console.log("Response for Column");
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var specs = JSON.parse(xhr.responseText);
                    //console.log(specs);
                    that.unloadColumn(index);
                    //console.log("after delete")
                    //that.print();
                    //console.log("after delete  asa ")
                    specs.sort(that.columnListComparator);
                    for (var i = 0 ; i< that.lattice.length; i++) {
                        //that.print();
                        if (index == 0) {
                            that.lattice[i].unshift(that.processSegmentData(specs[i]));
                        } else if( index == 6) {
                            that.lattice[i].push(that.processSegmentData(specs[i]));
                        } else {
                            console.log("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
                        }
                    }
                }
                that.dataLoading = false;
                /*console.log("After Column was over it");
                that.print();
                console.log("After Row was over it#######################");*/
            }
        };
        xhr.send();
    }

    manager.prototype.update = function() {
        if (this.dataLoading || this.debug) return;
        var object = player.object;
        if (object) {
            if (!this.objectSegment) {
                this.objectSegment = this.getSegmentByObject(object);
            } else {
                var segment = this.lattice[this.objectSegment.i][this.objectSegment.j];
                //var segment = this.objectSegment.segment;
                if (segment) {
                    var x = parseInt(object.x / this.multiplier);
                    var y = parseInt(object.y / this.multiplier);
                    if (segment.x != x || segment.y != y) {
                        this.objectSegment = this.getSegmentByObject(object);
                        //console.log(this.objectSegment);
                        if (this.objectSegment.i == 1) {
                            this.loadRow(0, segment.y,y+2);
                        } else if (this.objectSegment.i == 5) {
                            this.loadRow(6, segment.y,y-2);
                        }

                        if (this.objectSegment.j == 1) {
                            this.loadColumn(0, segment.x, x-2);
                        } else if (this.objectSegment.j == 5) {
                            this.loadColumn(6, segment.x, x+2);
                        }
                    }
                } else {
                    console.log("I HAS DUMB");
                }
            }
        }
    }

    manager.prototype.getSegmentByObject = function (object) {
        var x = parseInt(object.x / 1000);
        var y = parseInt(object.y / 1000);
        //this.print();
        for (var i= 0; i < this.lattice.length; i++) {
            for (var j = 0; j < this.lattice[i].length; j++) {
                var segment = this.lattice[i][j];
                if (x == segment.x && y == segment.y) {
                    return {i: i, j: j, segment: segment};
                }
            }
        }
    }

    manager.prototype.print = function () {
        for (var i = 0; i < this.lattice.length; i++) {
            var message = '';
            for (var j = 0; j < this.lattice[i].length; j++) {
                message += this.lattice[i][j].x + ":" + this.lattice[i][j].y  + '|';
            }
            console.log(message);
        }
    }

    return manager;
});
