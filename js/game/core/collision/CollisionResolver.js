define([], function() {
    var resolver = function(objectManager) {
       this.objectManager = objectManager;
    }

    resolver.prototype.resolvePairs = function(pairs) {
        if (pairs == null) return;
        for (var i = 0; i < pairs.length; i++) {
            this.resolvePair(pairs[i]);
        }
    }

    resolver.prototype.resolvePair = function(pair) {
        pair.o1.onCollision(pair.o2);
        pair.o2.onCollision(pair.o1);
        pair.o1.applyCollisionChanges(this.objectManager);
        pair.o2.applyCollisionChanges(this.objectManager);
    }

    return resolver;
});
