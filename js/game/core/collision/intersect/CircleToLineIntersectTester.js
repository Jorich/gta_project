define(['game/core/collision/Line'], function (Line) {
    var tester = function() {
        this.line = new Line(this, {x:0, y:0}, {x:2, y:0});
        //http://doswa.com/2009/07/13/circle-segment-intersectioncollision.html
        //http://bl.ocks.org/milkbread/11000965
    }

    tester.prototype.test = function(object1, object2) {
        var circle = this.getCircleBoundary(object1, object2);
        var polygon = this.getPolygonBoundary(object1, object2);
        var points = polygon.getPoints();

        for (var i = 0; i < points.length; i++) {
            if (i == points.length - 1) {
                this.line.points[0] = points[0];
                this.line.points[1] = points[i];
            } else {
                this.line.points[0] = points[i];
                this.line.points[1] = points[i + 1];
            }
            if (this.testAgainstLine(this.line, circle)) {
                return true;
            }
        }

        return false;
    }

    tester.prototype.testAgainstLine = function(line, circle) {
        // Calculate the euclidean distance between a & b
        eDistAtoB = line.length();

        // compute the direction vector d from a to b
        d = [
            (line.points[0].x - line.points[1].x)/eDistAtoB,
            (line.points[0].y - line.points[1].y)/eDistAtoB ];

        // Now the line equation is x = dx*t + ax, y = dy*t + ay with 0 <= t <= 1.
        // compute the value t of the closest point to the circle center (cx, cy)

        t = (d[0] * (circle.center.x - line.points[0].x)) + (d[1] * (circle.center.y - line.points[0].y));

        // compute the coordinates of the point e on line and closest to c
        var e = {coords:[]};
        e.coords[0] = (t * d[0]) + line.points[0].x;
        e.coords[1] = (t * d[1]) + line.points[0].y;

        // Calculate the euclidean distance between c & e

        eDistCtoE = Math.sqrt( Math.pow(e.coords[0]-circle.center.x, 2) + Math.pow(e.coords[1]-circle.center.y, 2) );

        // test if the line intersects the circle

        if( eDistCtoE < circle.radius ) {
            //return this.is_on(line,circle);
            dt = Math.sqrt( Math.pow(circle.radius, 2) - Math.pow(eDistCtoE, 2));
            return this.is_on(line, {x:((t-dt) * d[0]) + line.points[0].x,y:((t-dt) * d[1]) + line.points[0].y})
                || this.is_on(line, {x:((t+dt) * d[0]) + line.points[0].x,y:((t+dt) * d[1]) + line.points[0].y});

        }/* else if (parseInt(eDistCtoE) === parseInt(circle.radius)) {
            return false;
        }*/

        return false;
    }
    tester.prototype.getCircleBoundary = function(object1, object2) {
        if (object1.getBoundary().type == 'circle') {
            return object1.getBoundary();
        }
        if (object2.getBoundary().type == 'circle') {
            return object2.getBoundary();
        }
    }

    tester.prototype.getPolygonBoundary = function(object1, object2) {
        if (object1.getBoundary().type == 'polygon') {
            return object1.getBoundary();
        }
        if (object2.getBoundary().type == 'polygon') {
            return object2.getBoundary();
        }
    }

    // BASIC GEOMETRIC functions
    tester.prototype.distance = function(v1,v2) {
        return Math.sqrt( Math.pow(v1.x-v2.x, 2) + Math.pow(v1.y-v2.y, 2) )
    }
    tester.prototype.is_on = function(line, point) {
        return this.distance(line.points[0],point) + this.distance(point,line.points[1]) == this.distance(line.points[0],line.points[1]);
    }

    return tester;
});