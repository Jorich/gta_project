define(['game/core/collision/Line'], function(Line) {
    var tester = function () {
        this.line1 = new Line(this, {x:0, y:0}, {x:2, y:0});
        this.line2 = new Line(this, {x:1, y:-1}, {x:1, y:1});
    }

    tester.prototype.test = function (object1, object2) {
        var points1 = object1.getBoundary().points;
        var points2 = object2.getBoundary().points;

        for (var i = 0; i < points1.length; i++) {
            for (var j = 0; j < points2.length; j++) {
                if (i == points1.length - 1) {
                    this.line1.points[0] = points1[0];
                    this.line1.points[1] = points1[i];
                } else {
                    this.line1.points[0] = points1[i];
                    this.line1.points[1] = points1[i + 1];
                }

                if (j == points2.length - 1) {
                    this.line2.points[0] = points2[0];
                    this.line2.points[1] = points2[j];
                } else {
                    this.line2.points[0] = points2[j];
                    this.line2.points[1] = points2[j + 1];
                }
                if (this.line1.intersectsWith(this.line2)) {
                    //console.log("Intersection " +  object1.id + " : " + object2.id);
                    return true;
                }
            }
        }
        //console.log("No Intersection");
        return false;
    }

    return tester;
});
