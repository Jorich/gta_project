define([], function( ) {
    var tester = function() {

    }

    tester.prototype.test = function (object1, object2) {
        circle1 = object1.getBoundary();
        circle2 = object2.getBoundary();

        vector  = {
            x: circle1.center.x - circle2.center.x,
            y: circle1.center.y - circle2.center.y
        }

        var length = Math.sqrt(vector.x * vector.x + vector.y * vector.y);

        return length <= circle1.radius || length <= circle2.radius;
    }

    return tester;
});
