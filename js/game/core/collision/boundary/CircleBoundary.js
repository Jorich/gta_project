define([], function () {
    var c = function (center,r) {
        this.center = center;
        this.radius = r;
        this.type = 'circle';
    }

    c.prototype.setX = function(x) {
        this.center.x = x;
    }

    c.prototype.setY = function(y) {
        this.center.y = y;
    }

    c.prototype.update = function (updates) {
        this.center.x += updates.x;
        this.center.y += updates.y;
    }

    c.prototype.toSpec = function () {
        return {
            type: this.type,
            radius: this.radius
        }
    }

    return c;
})
