define(['game/core/collision/AABoundingBox'], function(AABoundingBox) {
    var Node = function(boundingBox, depth) {
        this.limit = 10;
        this.boundingBox = boundingBox;
        this.count = 0;
        this.children = [];
        if (depth == undefined) {
            this.depth = 0;
        } else {
            this.depth = depth;
        }

        this.staticPoints = []; // for buildings, trees etc.
        this.projectilePoints = []; // for projectiles
        this.dynamicPoints = []; // for vehicles and people
    }


    Node.prototype.queryRange = function (box, type, result) {

        if (box == null || !this.boundingBox.intersectsWith(box)) {
            return;
        }

        if (this.children.length > 0) {
            for (var i = 0; i < this.children.length; i++) {
                 this.children[i].queryRange(box, type, result);
            }

        } else {
            this.checkIfContains(box, this.dynamicPoints, result);
            this.checkIfContains(box, this.staticPoints, result);
            this.checkIfContains(box, this.projectilePoints, result);
        }

    }

    Node.prototype.checkIfContains = function (box, list, result) {
        for (var i = 0; i < list.length; i++) {
            if (box.containsPoint(list[i])) {
                var object = list[i].object;
                result[object.id] = object
            }
        }
    }

    Node.prototype.subDivide = function () {

        var subWidth = this.boundingBox.halfDimensions.x/2;
        var subHeight  = this.boundingBox.halfDimensions.y/2;

        this.children.push(new Node(new AABoundingBox(
            this.boundingBox.center.x + subWidth,
            this.boundingBox.center.y + subHeight,
            subWidth,
            subHeight
        ), this.depth+ 1));

        this.children.push(new Node(new AABoundingBox(
            this.boundingBox.center.x - subWidth,
            this.boundingBox.center.y + subHeight,
            subWidth,
            subHeight
        ), this.depth+ 1));

        this.children.push(new Node(new AABoundingBox(
            this.boundingBox.center.x - subWidth,
            this.boundingBox.center.y - subHeight,
            subWidth,
            subHeight
        ), this.depth+ 1));

        this.children.push(new Node(new AABoundingBox(
            this.boundingBox.center.x + subWidth,
            this.boundingBox.center.y - subHeight,
            subWidth,
            subHeight
        ), this.depth+ 1));


        for (var i = 0; i < this.dynamicPoints.length; i++) {
            this.addDynamic(this.dynamicPoints[i]);
        }

        for (var i = 0; i < this.staticPoints.length; i++) {
            this.addStatic(this.staticPoints[i]);
        }

        for (var i = 0; i < this.projectilePoints.length; i++) {
            this.addProjectile(this.projectilePoints[i]);
        }

        this.dynamicPoints = [];
        this.staticPoints = [];
        this.projectilePoints = [];
        this.count = 0;
    }



    Node.prototype.addProjectile = function(point) {

        if (!this.boundingBox.containsPoint(point)) return;

        if (this.children.length > 0) {
            this.children[0].addProjectile(point);
            this.children[1].addProjectile(point);
            this.children[2].addProjectile(point);
            this.children[3].addProjectile(point);

            return this;
        } else if (this.count >= this.limit) {
            this.subDivide();
            this.addProjectile(point);

            return this;
        } else {
            this.projectilePoints.push(point);
            this.count += 1;
            return this;
        }
    }

    Node.prototype.addDynamic = function (point) {
        if (!this.boundingBox.containsPoint(point)) return;

        if (this.children.length > 0) {
            this.children[0].addDynamic(point);
            this.children[1].addDynamic(point);
            this.children[2].addDynamic(point);
            this.children[3].addDynamic(point);

            return this;
        } else if (this.count >= this.limit) {
            this.subDivide();
            this.addDynamic(point);

            return this;
        } else {
            this.dynamicPoints.push(point);
            this.count += 1;

            return this;
        }
    }

    Node.prototype.addStatic = function ( point ) {

        if (!this.boundingBox.containsPoint(point)) return;
        if (this.children.length > 0) {
            this.children[0].addStatic(point);
            this.children[1].addStatic(point);
            this.children[2].addStatic(point);
            this.children[3].addStatic(point);

            return this;
        } else if (this.count >= this.limit) {
            this.subDivide();
            this.addStatic(point);

            return this;
        } else {
            this.staticPoints.push(point);
            this.count += 1;

            return this;
        }
    }

    return Node;
});
