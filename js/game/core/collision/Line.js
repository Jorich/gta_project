define([], function() {
    var l = function (object, p1, p2) {
        this.points = [p1, p2];
        this.object = object;
    }

    l.prototype.intersectsWith = function (line) {
        var p0_x = this.points[0].x;
        var p1_x = this.points[1].x;
        var p2_x = line.points[0].x;
        var p3_x = line.points[1].x;
        var p0_y = this.points[0].y;
        var p1_y = this.points[1].y;
        var p2_y = line.points[0].y;
        var p3_y = line.points[1].y;

        var s1_x, s1_y, s2_x, s2_y;
        s1_x = p1_x - p0_x;
        s1_y = p1_y - p0_y;
        s2_x = p3_x - p2_x;
        s2_y = p3_y - p2_y;

        var s, t;
        s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
        t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

        return s >= 0 && s <= 1 && t >= 0 && t <= 1;
    }

    l.prototype.intersectsLine = function (line) {
        var p1 = this.points[0];
        var p2 = this.points[1];
        var p3 = line.points[0];
        var p4 = line.points[1];

        var x=((p1.x*p2.y-p1.y*p2.x)*(p3.x-p4.x)-(p1.x-p1.x)*(p3.x*p4.y-p3.y*p4.x))/((p1.x-p2.x)*(p3.y-p4.y)-(p1.y-p2.y)*(p3.x-p4.x));
        var y=((p1.x*p2.y-p1.y*p2.x)*(p3.y-p4.y)-(p1.y-p2.y)*(p3.x*p4.y-p3.y*p4.x))/((p1.x-p2.x)*(p3.y-p4.y)-(p1.y-p2.y)*(p3.x-p4.x));
        if (isNaN(x)||isNaN(y)) {
            return false;
        } else {
            if (p1.x>=p2.x) {
                if (!(p2.x<=x&&x<=p1.x)) {return false;}
            } else {
                if (!(p1.x<=x&&x<=p2.x)) {return false;}
            }
            if (p1.y>=p2.y) {
                if (!(p2.y<=y&&y<=p1.y)) {return false;}
            } else {
                if (!(p1.y<=y&&y<=p2.y)) {return false;}
            }
            if (p3.x>=p4.x) {
                if (!(p4.x<=x&&x<=p3.x)) {return false;}
            } else {
                if (!(p3.x<=x&&x<=p4.x)) {return false;}
            }
            if (p3.y>=p4.y) {
                if (!(p4.y<=y&&y<=p3.y)) {return false;}
            } else {
                if (!(p3.y<=y&&y<=p4.y)) {return false;}
            }
        }
        return true;
    }

    l.prototype.length = function() {
        return Math.sqrt(Math.pow(this.points[0].x - this.points[1].x,2) + Math.pow(this.points[0].y - this.points[1].y,2))
    }

    return l;
})
