define([], function() {
    var box = function(x , y, h_x, h_y) {
        this.center = {
            x: x,
            y: y
        }
        this.halfDimensions = {
            x: h_x,
            y: h_y
        }
    }

    box.prototype.containsPoint = function(point) {

        var leftTop = {x: this.center.x - this.halfDimensions.x, y: this.center.y + this.halfDimensions.y};
        return (point.x >= leftTop.x
        && point.x <= leftTop.x + this.halfDimensions.x * 2
        && point.y <= leftTop.y
        && point.y >= leftTop.y - this.halfDimensions.y * 2);
    }

    box.prototype.intersectsWith = function (other) {
        return (Math.abs(this.center.x - other.center.x) * 2 < this.halfDimensions.x*2 + other.halfDimensions.x*2
        && Math.abs(this.center.y - other.center.y) * 2 < this.halfDimensions.y *2 + other.halfDimensions.y*2);
    }

    return box;
});
