define([], function () {
    var b = function (points, x, y) {
        this.type = 'polygon';
        this.x = x;
        this.y = y;
        this.rotation = 0;
        this.points = points;
    }

    b.prototype.setX = function(x) {
        for (var i = 0; i< this.points.length; i++) {
            diff = this.points[i].x - this.x;
            this.points[i].x += x;
        }
        this.x = x;

    }

    b.prototype.setY = function(y) {
        for (var i = 0; i < this.points.length; i++) {
            diff = this.points[i].y - this.y;
            this.points[i].y += y;
        }
        this.y = y;

    }

    b.prototype.update = function (updates) {
        var rotateBy;
        if (updates.rotation != this.rotation) {
            rotateBy = updates.rotation - this.rotation; //* (180 / Math.PI); JUST DONT!
            this.rotation = updates.rotation;
        }

        for (var i = 0; i < this.points.length; i++) {
            if (updates.x != 0 || updates.y != 0) {
                this.points[i].x += updates.x;
                this.points[i].y += updates.y;
            }
            if (rotateBy) {
                this.rotatePoint(this.points[i], rotateBy);
            }
        }
    }

    b.prototype.rotatePoint = function (p, angle) {
        var s = Math.sin(angle);
        var c = Math.cos(angle);

        // translate point back to origin:
        p.x -= this.x;
        p.y -= this.y;

        // rotate point
        var xnew = p.x * c - p.y * s;
        var ynew = p.x * s + p.y * c;

        // translate point back:
        p.x = xnew + this.x;
        p.y = ynew + this.y;
    }

    b.prototype.getPoints = function () {
        return this.points;
    }

    b.prototype.toSpec = function () {
        var resultingPoints = [];
        for (var i = 0; i < this.points.length; i++) {
            resultingPoints.push({x:this.points[i].x, y: this.points[i].y});
        }

        return {
            type: this.type,
            points: resultingPoints
        }
    }

    return b;
});
