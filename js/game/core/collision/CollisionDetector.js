define(
    [
        'game/core/collision/quadtree/Node',
        'game/core/collision/intersect/LineToLineIntersectTester',
        'game/core/collision/intersect/CircleToCircleIntersectTester',
        'game/core/collision/intersect/CircleToLineIntersectTester',
        'game/core/collision/AABoundingBox'
    ],
    function(Node, LineToLineIntersectTester, CircleToCircleIntersectTester, CircleToLineIntersectTester, AABoundingBox) {

    var detector = function(objectManager) {
        this.objectManager = objectManager;
        this.container = objectManager.container;
        this.lineToLineChecker = new LineToLineIntersectTester();
        this.circleToLineChecker = new CircleToLineIntersectTester();
        this.circleToCircleChecker = new CircleToCircleIntersectTester();
        this.boundingBox = new AABoundingBox(0,0,1000,1000);
    }

    detector.prototype.getCollisionPairs = function () {
        this.resetTree();
        pairs = [];
        for (var i in this.container) {
            if (this.container.hasOwnProperty(i)) {
                this.addObjectToTree(this.container[i]);
            }
        }

        for (var i in this.container) {
            if (this.container.hasOwnProperty(i)) {
                var object = this.container[i];
                var objects = {};
                this.tree.queryRange(object.getBoundingBox(), object.type, objects);
                for (var j in objects) {
                    if (objects.hasOwnProperty(j) && object.id != objects[j].id && this.checkIfCollides(object, objects[j])) {
                        pairs.push({o1: object, o2: objects[j]});
                    }
                }
                if (typeof object.handleProximity == "function") {
                    object.handleProximity(objects);
                }
            }
        }

        return pairs;
    }

    detector.prototype.checkIfCollides = function (object1, object2) {
        return this.getIntersectTester(object1, object2).test(object1, object2);
    }

    detector.prototype.getIntersectTester = function (o1, o2) {

        if (o1.getBoundary().type == "polygon" && o2.getBoundary().type == "polygon") {
            return this.lineToLineChecker;
        }
        if (o1.getBoundary().type == "circle" && o2.getBoundary().type == "circle") {
            return  this.circleToCircleChecker;
        }

        return this.circleToLineChecker;
    }

    detector.prototype.resetTree = function () {
        if (player.object) {
            this.boundingBox.center.x = player.object.x;
            this.boundingBox.center.y = player.object.y;
        }
        this.tree = new Node(this.boundingBox, 0);
    }

    detector.prototype.addObjectToTree = function ( object ) {
        if (object.type == "p") {
            if (object.boundary.type == "polygon") {
                for (var i = 0; i < object.boundary.points.length; i ++ ) {
                    this.tree.addDynamic( object.boundary.points[i])
                }
            } else { // Circle
                this.tree.addDynamic(object.boundary.center);
            }
        }

        if (object.type == "b") {
            for (var i = 0; i < object.boundary.points.length; i ++ ) {
                this.tree.addStatic(object.boundary.points[i]);
            }
        }

        if (object.type == "v") {
            for (var i = 0; i < object.boundary.points.length; i ++ ) {
                this.tree.addDynamic(object.boundary.points[i]);
            }
        }

        if (object.type == "proj") {
            if (object.boundary.type == "polygon") {
                for (var i = 0; i < object.boundary.points.length; i ++ ) {
                    this.tree.addProjectile( object.boundary.points[i])
                }
            } else { // Circle
                this.tree.addProjectile (object.boundary.center);
            }
        }

        if (object.type == "e") {
            this.tree.addProjectile(object.boundary.center);
        }
    }

    return detector;
});
