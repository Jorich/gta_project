define([], function() {
    var v = function(object, x, y) {
        this.x = x;
        this.y = y;
        this.object = object;
    }

    return v;
})
