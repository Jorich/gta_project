importScripts('../lib/require.js');
require.config({
    paths: {
        three: 'lib/three.min'
    }
});
require({baseUrl: "../"},
    [
        "game/core/worker/Player",
        "game/core/message/worker/Manager",
        "game/core/object/worker/Manager",
        "game/core/controls/worker/Interpreter",
        "game/core/collision/CollisionDetector",
        "game/core/collision/CollisionResolver",
        "game/core/worker/SegmentManager",
        "three"
    ],
    function(
        Player,
        MessageManager,
        ObjectManager,
        ControlInterpreter,
        CollisionDetector,
        CollisionResolver,
        SegmentManager
        ){

        player = new Player();
        objectManager = new ObjectManager();
        controls = new ControlInterpreter(objectManager);
        controls.setPlayer(player);
        player.setControls(controls);
        messageManager = new MessageManager();
        messageManager.setPlayer(player);
        collisionResolver = new CollisionResolver(objectManager);
        collisionDetection = new CollisionDetector(objectManager);
        messageManager.setControls(controls);
        messageManager.setObjectManager(objectManager);
        segmentManager = new SegmentManager(messageManager, objectManager);
        //segmentManager.debug = true;

        messageManager.init();

        var dt = 25 / 1000;

        setInterval(function() {
            objectManager.update(dt);
            collisionResolver.resolvePairs(collisionDetection.getCollisionPairs());
            if (objectManager.pendingUpdates.length > 0 ) {
                postMessage({type: "object", subType:"batch_update", objects: objectManager.objectsToSpec(objectManager.pendingUpdates)});
                objectManager.clearUpdates();
            }
         }, 25);

        setInterval(function() {
            segmentManager.update();
        },100);

        isPlayer = function (o) {
            return o.id == "player_1";
        }
    }
);
