<?php
require_once "functions.php";

$lattice =array(
    array(),
    array(),
    array(),
    array(),
    array(),
    array(),
    array(),
);

for ($i = 3; $i >= -3; $i--) {
    for ($j = -3; $j <= 3; $j++) {
        if ($i == 0 && $j == 0) {
            $chunk = createChunk($j,$i);
            $chunk->player = createPerson(0,0, 'player_1');
            $lattice[$i+3][] = $chunk;
        } else {
            $lattice[$i+3][] = createChunk($j,$i);
        }
    }
}

echo json_encode($lattice);
